import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

import java.util.Objects;

/**
 * A collection of all request handler supported by the MiniServer
 */
public class RequestHandlingManager {

    private static RequestHandlingManager requestHandlingManager;

    public static RequestHandlingManager getInstance()   {
        if (requestHandlingManager == null)    {
            requestHandlingManager = new RequestHandlingManager();
        }
        return requestHandlingManager;
    }

    public RequestHandler getStoreFileFromClientHandler() {
        return new StoreFileFromClientHandler();
    }

    public RequestHandler getFileGetRequestFromClientHandler() {
        return new FileGetRequestFromClientHandler();
    }

    public RequestHandler getFileStorageRequestHopHandler() {
        return new FileStorageRequestHopHandler();
    }

    public HelloPingProcessingHandler getHelloPingProcessingHandler() {
        return new HelloPingProcessingHandler();
    }

    public RequestHandler getKeyBackupHandler() {
        return new KeyBackupHandler();
    }

    public RequestHandler getReceivePendingKeysHandler() {
        return new ReceivePendingKeysHandler();
    }
}

/**
 * Store a file in the p2p network
 */
class StoreFileFromClientHandler implements RequestHandler {

    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.PEER_RPC_METHOD_STORE_FILE};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        FileToStore fileToStore = FileToStore.fromObject(request.getID());
        ConsoleHelper.print("received file to store: ", fileToStore.toString());
        Integer hash = Math.abs(Objects.hash(fileToStore)%((int)Math.pow(2, BaseConfig.MAX_BITS)));
        ConsoleHelper.print("hash of the file: ", Integer.toString(hash));
        RoutingTable.getInstance().attemptToStore(hash);
        return new JSONRPC2Response(hash, request.getID());
    }
}

/**
 * Get a file from the p2p network
 */
class FileGetRequestFromClientHandler implements RequestHandler {

    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.PEER_RPC_METHOD_GET_FILE};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        int hash = Integer.parseInt(request.getID().toString());
        ConsoleHelper.print("request to get file for hash: ", Integer.toString(hash));
        String clientIp = request.getNamedParams().getOrDefault(BaseConfig.RPC_PARAM_CLIENT_IP, "").toString();
        ConsoleHelper.print("got client ip: ", clientIp);
        if (clientIp.isEmpty()) {
            ConsoleHelper.print("invalid client ip!");
            return null;
        }
        RoutingTable.getInstance().attemptToGetFile(hash, clientIp);
        return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
    }
}

/**
 * This handlwe checks if the given key belongs to itself and stores it if it does.
 * Otherwise it forwards the key to the next possible closest peer.
 */
class FileStorageRequestHopHandler implements RequestHandler {
    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.PEER_RPC_METHOD_HOP_HANDLER};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        Integer key = Integer.parseInt(request.getID().toString());
        RoutingTable.getInstance().attemptToStore(key);
        return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
    }
}

/**
 * This handlwe checks if the given key belongs to itself and stores it if it does.
 * Otherwise it forwards the key to the next possible closest peer.
 */
class ReceivePendingKeysHandler implements RequestHandler {
    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.PEER_RPC_METHOD_RECEIVE_PENDING_KEYS};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        Integer key = Integer.parseInt(request.getID().toString());
        RoutingTable.getInstance().attemptToStore(key);
        return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
    }
}

/**
 * Handler to update the current routing table when someone says hello
 */
class HelloPingProcessingHandler implements RequestHandler {
    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.PEER_RPC_METHOD_HELLO_PING};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        Guuid senderGuuid = Guuid.fromObject(request.getID().toString());
        ConsoleHelper.print("HELLO PING RECEIVED from: " + senderGuuid.toString());
        RoutingTable.getInstance().refresh();
        RoutingTable.getInstance().giveBackKeysToNewPeers();
        return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
    }
}

/**
 * Backup the incoming key in the current storage or forward it
 */
class KeyBackupHandler implements RequestHandler {
    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.PEER_RPC_METHOD_BACKUP_KEY};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        Integer key = Integer.parseInt(request.getID().toString());
        RoutingTable.getInstance().refresh();
        RoutingTable.getInstance().attemptToStore(key);
        return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
    }
}