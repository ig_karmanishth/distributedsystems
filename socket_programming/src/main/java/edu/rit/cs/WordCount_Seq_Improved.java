package edu.rit.cs.basic_word_count;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordCount_Seq_Improved {

    public static String TEMP_FILENAME_TO_STORE_RECEIVED_DATA = "tempFile.csv";
    /**
     * Read and parse all reviews
     * @param dataset_file
     * @return list of reviews
     */
    private List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * Get a string equivalent of the results
     * @param wordcount
     */
    private String getWordCountAsString(Map<String, Integer> wordcount){
        StringBuilder completeString = new StringBuilder();
        for(String word : wordcount.keySet()){
            completeString.append(word + " : " + wordcount.get(word).toString() + '\n');
        }
        return completeString.toString();
    }

    /**
     * Emit 1 for every word and store this as a <key, value> pair
     * @param allReviews
     * @return
     */
    private List<KV<String, Integer>> map(List<AmazonFineFoodReview> allReviews) {
        List<KV<String, Integer>> kv_pairs = new ArrayList<KV<String, Integer>>();

        for(AmazonFineFoodReview review : allReviews) {
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while(matcher.find())
                kv_pairs.add(new KV(matcher.group().toLowerCase(), 1));
        }
        return kv_pairs;
    }
    
    /**
     * count the frequency of each unique word
     * @param kv_pairs
     * @return a list of words with their count
     */
    private Map<String, Integer> reduce(List<KV<String, Integer>> kv_pairs) {
        Map<String, Integer> results = new HashMap<>();

        for(KV<String, Integer> kv : kv_pairs) {
            if(!results.containsKey(kv.getKey())) {
                results.put(kv.getKey(), kv.getValue());
            } else{
                int init_value = results.get(kv.getKey());
                results.replace(kv.getKey(), init_value, init_value+kv.getValue());
            }
        }
        return results;
    }


    public void startCounting(String data) {
        try {
            File tempFile = new File(TEMP_FILENAME_TO_STORE_RECEIVED_DATA);
            if (tempFile.createNewFile()) {
                FileWriter writer = new FileWriter(tempFile);
                writer.write(data);
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("FAILED! Temp file cannot be created.");
        }
        List<AmazonFineFoodReview> allReviews = read_reviews(TEMP_FILENAME_TO_STORE_RECEIVED_DATA);
        System.out.println("Finished reading all reviews, now performing word count...");

        MyTimer myMapTimer = new MyTimer("map operation");
        myMapTimer.start_timer();
        List<KV<String, Integer>> kv_pairs = map(allReviews);
        myMapTimer.stop_timer();


        MyTimer myReduceTimer = new MyTimer("reduce operation");
        myReduceTimer.start_timer();
        Map<String, Integer> results = reduce(kv_pairs);
        myReduceTimer.stop_timer();
        myReduceTimer.print_elapsed_time();

        String result = getWordCountAsString(results);
//        if (saveResultToFile(result))   {
//            System.out.println("SAVED SUCCESSFULLY");
//        }else   {
//            System.out.println("FAILED TO SAVE");
//        }
        System.out.println(result);
        myMapTimer.print_elapsed_time();
        myReduceTimer.print_elapsed_time();
    }

    private static boolean saveResultToFile(String result) {
        File file = new File("results.csv");
        try {
            if (file.createNewFile()) {
                FileWriter writer = new FileWriter(file);
                writer.write(result);
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
