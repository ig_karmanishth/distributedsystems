import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.Dispatcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * A singleton worker thread manager that contains all the worker
 * thread classes operational on the EventManager server
 *
 * These classes are namely:
 * - SaveNewTopicWorker
 * - TopicSupplierWorker
 */
public class RpcServerWorkerThreadManager {
    private static RpcServerWorkerThreadManager rpcServerWorkerThreadManager;
    public static RpcServerWorkerThreadManager getInstance()    {
        if (rpcServerWorkerThreadManager == null)   {
            rpcServerWorkerThreadManager = new RpcServerWorkerThreadManager();
        }
        return rpcServerWorkerThreadManager;
    }

    /**
     * Get a worker that stores file into the p2p system
     * @param socket
     * @return
     */
    public StoreFileWorker getStoreFileWorker(Socket socket) {
        return new StoreFileWorker(socket);
    }

    /**
     * Get a worker that returns a stored file from the p2p system
     * @param socket
     * @return
     */
    public GetFileWorker getGetRequestFileWorker(Socket socket) {
        return new GetFileWorker(socket);
    }

    /**
     * Get a worker that updates the routing table when a new peer says hello
     * @param socket
     * @return
     */
    public HelloPingProcessingWorker getHelloPingProcessingWorker(Socket socket) {
        return new HelloPingProcessingWorker(socket);
    }

    /**
     * Get a worker that stores file into the p2p system
     * @param socket
     * @return
     */
    public FileStoreHopRequestWorker getFileStoreHopRequestWorker(Socket socket) {
        return new FileStoreHopRequestWorker(socket);
    }

    public BackupRequestListeningWorker getBackupRequestListeningWorker(Socket socket) {
        return new BackupRequestListeningWorker(socket);
    }

    public ReceivePendingKeysWorker getReceivePendingKeysWorker(Socket socket) {
        return new ReceivePendingKeysWorker(socket);
    }
}

/**
 * A worker thread to store a new file in the p2p network
 */
class StoreFileWorker extends Thread  {
    private static final String TAG = StoreFileWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public StoreFileWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(RequestHandlingManager.getInstance().getStoreFileFromClientHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            out.flush();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }
}

/**
 * A worker thread to return a file from the p2p network
 */
class GetFileWorker extends Thread  {
    private static final String TAG = GetFileWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public GetFileWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(RequestHandlingManager.getInstance().getFileGetRequestFromClientHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            out.flush();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }
}

/**
 * A worker thread to handle the incoming hopped request to store a file
 */
class FileStoreHopRequestWorker extends Thread  {
    private static final String TAG = FileStoreHopRequestWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public FileStoreHopRequestWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(RequestHandlingManager.getInstance().getFileStorageRequestHopHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            out.flush();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }
}

class HelloPingProcessingWorker extends Thread  {
    private static final String TAG = HelloPingProcessingHandler.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public HelloPingProcessingWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(RequestHandlingManager.getInstance().getHelloPingProcessingHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            out.flush();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }
}

/**
 * A worker thread to backup incoming keys
 */
class BackupRequestListeningWorker extends Thread  {
    private static final String TAG = BackupRequestListeningWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public BackupRequestListeningWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(RequestHandlingManager.getInstance().getKeyBackupHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            out.flush();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }
}

/**
 * A worker thread to handle pending keys from neighboring peers
 */
class ReceivePendingKeysWorker extends Thread  {
    private static final String TAG = ReceivePendingKeysWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public ReceivePendingKeysWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(RequestHandlingManager.getInstance().getReceivePendingKeysHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            out.flush();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }
}