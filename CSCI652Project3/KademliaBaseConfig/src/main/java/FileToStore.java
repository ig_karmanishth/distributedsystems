import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class FileToStore {
    String filename;

    public FileToStore(@JsonProperty("filename") String filename) {
        this.filename = filename;
    }

    public static FileToStore fromObject(Object fileToStore)    {
        try {
            return new ObjectMapper().readValue(fileToStore.toString(), FileToStore.class);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File mapping failed");
        }
        return null;
    }

    public String getFilename() {
        return filename;
    }

    @Override
    public String toString() {
        return "FileToStore{" +
                "filename='" + filename + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileToStore that = (FileToStore) o;
        return filename.equals(that.filename);
    }

    @Override
    public int hashCode() {
        return filename.length();
    }
}
