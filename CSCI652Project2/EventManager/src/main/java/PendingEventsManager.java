import java.io.Console;
import java.util.*;

/**
 * A Singleton manager that handles all pending events flowing through the EventManager system.
 * For all events whose subscriber is active, the events are sent and marked as sent right away.
 * All other unsent events remain queued up for being sent at a later time.
 */
public class PendingEventsManager {
    private static final String TAG = PendingEventsManager.class.getSimpleName() + ":";
    private static PendingEventsManager pendingEventsManager;
    // the boolean denotes if the event `isSent`
    Map<SubscriberModel, Map<Event, Boolean>> pendingEventMap;
    private final Object lockObject = new Object();

    public PendingEventsManager() {
        this.pendingEventMap = new HashMap<>();
    }

    public static PendingEventsManager getInstance()    {
        if (pendingEventsManager == null)   {
            pendingEventsManager = new PendingEventsManager();
        }
        return pendingEventsManager;
    }

    public Map<SubscriberModel, Map<Event, Boolean>> getPendingEventMap() {
        return pendingEventMap;
    }

    public boolean addPendingEvent(SubscriberModel subscriberModel, Event event)   {
        synchronized (this.lockObject) {
            if (this.pendingEventMap.containsKey(subscriberModel)) {
                Map<Event, Boolean> mappedEvents = this.pendingEventMap.getOrDefault(subscriberModel, null);
                if (mappedEvents == null) {
                    ConsoleHelper.printError(TAG, "Failure! This subscriber has null mapped events");
                    return false;
                } else {
                    // time to add the new event to the map
                    mappedEvents.put(event, false);
                }
            } else {
                // this is a brand new subscriber, add the new event
                Map<Event, Boolean> mappedEvents = new HashMap<>();
                mappedEvents.put(event, false);
                this.pendingEventMap.put(subscriberModel, mappedEvents);
            }
            ConsoleHelper.print(TAG, "new pending event...", event.toString());
            printPendingEvents();
            return true;
        }
    }

    /**
     * Remove all pending events for the given subscriber.
     * NOTE: This may happen in the case when the subscriber unsubscribed from a topic.
     * @param subscriberModel
     * @return
     */
    public boolean removePendingEvents(SubscriberModel subscriberModel)   {
        synchronized (this.lockObject) {
            this.pendingEventMap.remove(subscriberModel);
            return true;
        }
    }

    /**
     * Return all pending events for the subscriber with the given id
     * @param id
     * @return
     */
    public List<Event> getPendingEventsForSubscriberById(int id) {
        List<Event> pendingEvents = new ArrayList<>();
        this.pendingEventMap.entrySet().stream()
                // filter by subsriber id
                .filter(e -> e.getKey().getId() == id)
                .findFirst()
                .ifPresent(entry -> {
                    // extract the non-sent events
                    entry.getValue().forEach((key, value) -> {
                        if (!value) {
                            // if the value is false, this means that the event is not sent yet, so add it to the pending event list
                            pendingEvents.add(key);
                        }
                    });
                });
        return pendingEvents;
    }

    public synchronized boolean markPendingEventsAsSent(int id, List<Event> sentEvents)  {
        this.pendingEventMap.entrySet().stream()
                // filter by subsriber id
                .filter(e -> e.getKey().getId() == id)
                .findFirst()
                .ifPresent(entry -> {
                    // mark each element of sentEvents list as sent
                    entry.getValue().forEach((event, boolValue) -> {
                        if (sentEvents.contains(event))   {
                            SubscriberModel subscriberModel = getSubscriberFromId(id);
                            this.pendingEventMap.get(subscriberModel).put(event, true);
                        }
                    });
                });
        ConsoleHelper.print(TAG, "Marked", Integer.toString(sentEvents.size()), "event as sent!");
        printPendingEvents();
        return true;
    }

    private SubscriberModel getSubscriberFromId(int id)   {
        return pendingEventMap.keySet().stream()
                .filter(s -> s.getId() == id)
                .findFirst()
                .get();
    }

    private void printPendingEvents() {
        ConsoleHelper.printLine();
        ConsoleHelper.print("PENDING EVENTS");
        ConsoleHelper.printLine();
        this.pendingEventMap.forEach((subscriber, eventMapping) -> ConsoleHelper.print(subscriber.toString(), " : ", eventMapping.toString()));
        ConsoleHelper.printLine();
    }
}
