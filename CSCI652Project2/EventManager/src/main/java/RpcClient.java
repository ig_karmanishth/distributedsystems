import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RpcClient {
	private static final String TAG = RpcClient.class.getSimpleName();
	private JSONRPC2Session mySession;

	/**
	 * Singleton instance retriever
	 * @return
	 */
	public static RpcClient getNew(String serverURL)	{
		return new RpcClient().startSession(serverURL);
	}

	/**
	 * Start a client session to the given server IP address
	 */
	public RpcClient startSession(String serverURL) {
		URL parsedUrl = null;
		try {
			parsedUrl = new URL(serverURL);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			ConsoleHelper.exit();
		}
		mySession = new JSONRPC2Session(parsedUrl);
		return this;
	}

	/**
	 * Trigger event notification to the subscriber. Add the event to the
	 * {@link PendingEventsManager} if any failure occurs such as Subscriber non
	 * availability.
	 *
	 * @return
	 */
	public JSONRPC2Response sendEventNotification(Event event) throws JSONRPC2SessionException {
		String methodName = BaseConfig.RPC_METHOD_NAME_EVENT_NOTIFICATION;
		JSONRPC2Request request = new JSONRPC2Request(methodName, Math.abs(Objects.hash(Instant.now())%100000));
		Map<String, Object> myParams = new HashMap<>();
		myParams.put(BaseConfig.RPC_PARAM_EVENT, event);
		request.setNamedParams(myParams);
		return mySession.send(request);
	}
}