import com.fasterxml.jackson.databind.ObjectMapper;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

import java.io.IOException;

/**
 * A collection of all request handlers supported by the Subscriber
 */
public class RequestHandlingManager {

    private static RequestHandlingManager requestHandlingManager;

    public static RequestHandlingManager getInstance()   {
        if (requestHandlingManager == null)    {
            requestHandlingManager = new RequestHandlingManager();
        }
        return requestHandlingManager;
    }

    public RequestHandler getEventRequestHandler() {
            return new EventRequestHandler();
    }
}

/**
 * handle incoming event from EventManager
 */
class EventRequestHandler implements RequestHandler {
    private static final String TAG = EventRequestHandler.class.getSimpleName() + ":";

    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.RPC_METHOD_NAME_EVENT_NOTIFICATION};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        Object eventObject = request.getNamedParams().getOrDefault(BaseConfig.RPC_PARAM_EVENT, null);
        if (eventObject == null)    {
            ConsoleHelper.printError(TAG, "No valid event received in the request:", request.toString());
            return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE.getValue(), request.getID());
        }
        try {
            Event incomingEvent = new ObjectMapper().readValue(eventObject.toString(), Event.class);
            // print the received event on the console
            ConsoleHelper.printLine();
            ConsoleHelper.print(TAG, "New notification received: [ title:", incomingEvent.getTitle(), ", content:", incomingEvent.getContent(), "]");
            return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
        } catch (IOException e) {
            e.printStackTrace();
            return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE.getValue(), request.getID());
        }
    }
}