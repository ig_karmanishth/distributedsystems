import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PeerStatusManager {
    private static final String TAG = PeerStatusManager.class.getSimpleName() + ":";
    private static PeerStatusManager peerStatusManager;
    private HashMap<Guuid, StatusEnum> guuidToStatusMap = new HashMap<>();

    public static PeerStatusManager getInstance()    {
        if (peerStatusManager == null)    {
            peerStatusManager = new PeerStatusManager();
        }
        return peerStatusManager;
    }

    public synchronized void markOnline(Guuid nodeGuuid)  {
        guuidToStatusMap.put(nodeGuuid, StatusEnum.ONLINE);
        ConsoleHelper.print(TAG, "Marked online: ", nodeGuuid.toString());
        print();
    }

    public synchronized void markOffline(Guuid nodeGuuid)  {
        guuidToStatusMap.put(nodeGuuid, StatusEnum.OFFLINE);
        ConsoleHelper.print(TAG, "Marked offline: ", nodeGuuid.toString());
        print();
    }

    private void print()    {
        ConsoleHelper.print(toString());
    }

    @Override
    public String toString() {
        return "PeerStatusManager{" + guuidToStatusMap.toString() + "}";
    }

    public List<Guuid> getOnlinePeers() {
        List<Guuid> toReturn = guuidToStatusMap.entrySet().stream()
                .filter( entry -> entry.getValue() == StatusEnum.ONLINE)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        ConsoleHelper.print("returning online nodes: \n------");
        toReturn.forEach(guuid -> ConsoleHelper.print(guuid.toString() + "\n"));
        return toReturn;
    }
}
