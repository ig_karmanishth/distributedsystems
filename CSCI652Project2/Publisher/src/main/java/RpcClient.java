//The Client sessions package

//The Base package for representing JSON-RPC 2.0 messages

//The JSON Smart package for JSON encoding/decoding (optional)

//For creating URLs

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RpcClient {
	private static final String TAG = RpcClient.class.getSimpleName();
	private JSONRPC2Session mySession;

	/**
	 * Singleton instance retriever
	 * @return
	 */
	public static RpcClient getNew(String serverURL)	{
		return new RpcClient().startSession(serverURL);
	}

	/**
	 * Start a client session to the given server IP address
	 */
	public RpcClient startSession(String serverURL) {
		URL parsedUrl = null;
		try {
			parsedUrl = new URL(serverURL);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			ConsoleHelper.exit();
		}
		mySession = new JSONRPC2Session(parsedUrl);
		return this;
	}

	public JSONRPC2Response advertiseNewTopic(Topic topic, int requestId)	{
		String methodName = BaseConfig.RPC_METHOD_NAME_ADVERTISE_TOPIC;
		JSONRPC2Request request = new JSONRPC2Request(methodName, requestId);
		Map<String, Object> myParams = new HashMap<String, Object>();
		myParams.put(BaseConfig.RPC_PARAM_TOPIC, topic);
		request.setNamedParams(myParams);
		JSONRPC2Response response = null;
		try {
			response = mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
		}
		return response;
	}

	public List<Topic> getTopics(int requestId)	{
		String methodName = BaseConfig.RPC_METHOD_NAME_GET_TOPICS;
		JSONRPC2Request request = new JSONRPC2Request(methodName, requestId);
		JSONRPC2Response response = null;
		try {
			response = mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
		}
		List<Topic> topicList = null;
		try {
			topicList = new ObjectMapper().readValue(response.getResult().toString(), new TypeReference<List<Topic>>(){});
		} catch (IOException e) {
			ConsoleHelper.printError(TAG, ": Error parsing retrieved list of topic");
			e.printStackTrace();
		}
		return topicList;
	}

	public JSONRPC2Response publishNewEvent(Event event, int requestId) throws JSONRPC2SessionException {
		String methodName = BaseConfig.RPC_METHOD_NAME_PUBLISH_EVENT;
		JSONRPC2Request request = new JSONRPC2Request(methodName, requestId);
		Map<String, Object> myParams = new HashMap<String, Object>();
		myParams.put(BaseConfig.RPC_PARAM_EVENT, event);
		request.setNamedParams(myParams);
		JSONRPC2Response response = null;
		response = mySession.send(request);
		return response;
	}
}