import java.util.HashSet;
import java.util.Set;

/**
 * Manager to store keys for this node
 */
public class StorageManager {
    private static StorageManager mStorageManager;
    private Set<Integer> storedKeys = new HashSet<>();

    public static StorageManager getInstance(){
        if (mStorageManager == null){
            mStorageManager = new StorageManager();
        }
        return mStorageManager;
    }

    public synchronized void addKey(Integer key){
        this.storedKeys.add(key);
        ConsoleHelper.print("added key: ", key.toString());
        show();
    }

    public Set<Integer> getStoredKeys() {
        return storedKeys;
    }

    private void show() {
        ConsoleHelper.print(this.toString());
    }

    @Override
    public String toString() {
        return "StorageManager{" +
                "storedKeys=" + storedKeys +
                '}';
    }

    public synchronized void removeKey(Integer key) {
        this.storedKeys.remove(key);
        ConsoleHelper.print("added key: ", key.toString());
        show();
    }

    public boolean isEmpty() {
        return this.storedKeys.isEmpty();
    }
}
