import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

public class RpcClient {
	private static final String TAG = RpcClient.class.getSimpleName();
	private JSONRPC2Session mySession;

	/**
	 * Start a client session to the given server IP address
	 */
	public RpcClient(String serverURL) {
		URL parsedUrl = null;
		try {
			parsedUrl = new URL(serverURL);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			ConsoleHelper.exit();
		}
		mySession = new JSONRPC2Session(parsedUrl);
	}

	public JSONRPC2Response storeFile(FileToStore fileToStore) {
		String methodName = BaseConfig.PEER_RPC_METHOD_STORE_FILE;
		String serializedFile = null;
		try {
			serializedFile = new ObjectMapper().writeValueAsString(fileToStore);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			ConsoleHelper.printError("there was a problem serializing the file");
		}
		ConsoleHelper.print("sending serialized file: ", serializedFile);
		JSONRPC2Request request = new JSONRPC2Request(methodName, serializedFile);
		try {
			return mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
			ConsoleHelper.printError("there was a problem sending the file");
		}
		return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE);
	}

	public JSONRPC2Response getFile(String hash) {
		String methodName = BaseConfig.PEER_RPC_METHOD_GET_FILE;
		JSONRPC2Request request = new JSONRPC2Request(methodName, hash);
		Map<String, Object> paramMap = new HashMap<>();
		try {
			String myAddress = "http://" + InetAddress.getLocalHost().getHostAddress();
			ConsoleHelper.print("sending client host address: " + myAddress);
			paramMap.put(BaseConfig.RPC_PARAM_CLIENT_IP, myAddress);
			request.setNamedParams(paramMap);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			ConsoleHelper.printError("failed to retrieve client ip address");
		}
		try	{
		return mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
			return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE);
		}
	}
}