import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.EnumSet;

/**
 * A singleton shared configuration class that is shared across nodes
 * ranging from Publisher Subscribers to EventManager.
 *
 * It contains all the shared constant fields like
 * - RPC method names
 * - Access ports
 * - RPC parameter names
 */
public class BaseConfig {
    private static BaseConfig baseConfig;
    public static final String RPC_METHOD_NAME_ADVERTISE_TOPIC = "method_advertiseTopic";
    public static final String RPC_METHOD_NAME_GET_TOPICS = "method_getTopics";
    public static final String RPC_METHOD_NAME_PUBLISH_EVENT = "method_publishEvent";
    public static final String RPC_METHOD_NAME_LOGIN = "method_login";
    public static final String RPC_METHOD_NAME_SUBSCRIBE_TOPIC = "method_subscribeTopic";
    public static final String RPC_METHOD_NAME_UNSUBSCRIBE_TOPIC = "method_unsubscribeTopic";
    public static final String RPC_METHOD_NAME_UNSUBSCRIBE_ALL = "method_unsubscribeAll";
    public static final String RPC_METHOD_NAME_SUBSCRIBE_KEYWORD = "method_subscribeKeyword";
    public static final String RPC_METHOD_NAME_GET_MY_TOPICS = "method_getMyTopics";
    public static final String RPC_METHOD_NAME_EVENT_NOTIFICATION = "method_eventNotification";
    public static final String RPC_PARAM_TOPIC = "param_topic";
    public static final String RPC_PARAM_EVENT = "param_event";
    public static final String RPC_PARAM_UNIQUE_HOST_ID = "param_host_id";
    public static final String RPC_PARAM_HOST_IP = "param_host_ip";
    public static final String RPC_PARAM_KEYWORD = "param_keyword";
    public static final String IP_EVENT_MANAGER = "http://172.20.240.2";
    public static final int PORT_EVENT_MANAGER_TOPIC_ADVERTISEMENT = 9001;
    public static final int PORT_EVENT_MANAGER_EVENT_PUBLISH = 9003;
    public static final int PORT_EVENT_MANAGER_GET_TOPICS = 9002;
    public static final int PORT_EVENT_MANAGER_SUBSCRIPTION_LISTENING = 9008;
    public static final int PORT_EVENT_MANAGER_SUBSCRIBER_LOGIN = 9009;
    public static final int PORT_EVENT_MANAGER_UNSUBSCRIPTION_LISTENING = 9010;
    public static final int PORT_SUBSCRIBER_EVENT_NOTIFICATION = 9011;
    public static final int PORT_EVENT_MANAGER_PENDING_NOTIFICATIONS = 9007;
    public static final String JSON_OBJECT_KEY_NAME = "key_name";
    public static final String JSON_OBJECT_KEY_ID = "key_id";
    public static final String JSON_OBJECT_KEY_KEYWORDS = "key_keywords";

    public static int qos = 1;
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum ResponseEnum {
        SUCCESS(true),
        FAILURE(false);

        private boolean resultValue;

        ResponseEnum(boolean value) {
            this.resultValue = value;
        }

        public boolean getValue() {
            return resultValue;
        }
    }

    public static BaseConfig getInstance() {
        if (baseConfig == null) {
            baseConfig = new BaseConfig();
        }
        return baseConfig;
    }

    public QosCounter getNewQosCounter() {
        return new QosCounter();
    }
    /**
     * A qos counter class with a default retry limit that can be
     * used by any object that wants to retry transmission at the application
     * layer.
     */
    class QosCounter {
        private final String TAG = ">>" + QosCounter.class.getSimpleName() + ":";
        private int retryLimit;
        public QosCounter() {
            if (BaseConfig.qos <= 1)   {
                this.retryLimit = 1;
            } else if (BaseConfig.qos == 2) {
                this.retryLimit = 5;
            }
        }
        public boolean isRetryAttemptAvailable()   {
            ConsoleHelper.print(TAG, "Attempting transmission! # of QoS attempts available:", Integer.toString(this.retryLimit));
            return this.retryLimit-- > 0;
        }

        /**
         * By marking a successful transmission, we make sure that whoever
         * then calls isRetryAttemptAvailable is returned false
         */
        public void markSuccessfulTransmission()    {
            this.retryLimit = 0;
        }
    }
}
