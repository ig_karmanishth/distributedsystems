import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A model class to represent a globally unique peer id
 */
public class Guuid {
    private int guuid;
    private String binaryString = "";
    private String ip;

    public Guuid(@JsonProperty("guuid") int guuid,
                 @JsonProperty("binaryString") String binaryString,
                 @JsonProperty("ip") String ip) {
        this.guuid = guuid;
        this.binaryString = binaryString;
        this.ip = ip;
    }

    public static Guuid fromObject(String jsonString)  {
        try {
            return new ObjectMapper().readValue(jsonString, Guuid.class);
        } catch (IOException e) {
            e.printStackTrace();
            ConsoleHelper.printError("there was a problem in converting request to Guuid!");
        }
        return null;
    }
    public int getGuuid() {
        return guuid;
    }

    public String getBinaryString() {
        return binaryString;
    }

    public static String getBinaryString(int guuid) {
        return String.join("", asListOfString(guuid));
    }

    public String getIp() {
        if (ip.contains("http://")) {
            return ip;
        }
        return "http://" + ip;
    }

    public static List<String> asListOfString(int guuid) {
        List<String> listOfBinaryStrings = Arrays.stream(Integer.toBinaryString(guuid).split("")).collect(Collectors.toList());
        if (listOfBinaryStrings.size() == BaseConfig.MAX_BITS) {
            return listOfBinaryStrings;
        }
        List<String> listWithZeroes = new ArrayList<>(Collections.nCopies(BaseConfig.MAX_BITS - listOfBinaryStrings.size(), "0"));
        listWithZeroes.addAll(listOfBinaryStrings);
        return listWithZeroes;
    }

    public List<String> asListOfString() {
        return asListOfString(this.guuid);
    }

    @Override
    public String toString() {
        return "Guuid{" +
                "guuid=" + guuid +
                ", binaryString='" + binaryString + '\'' +
                ", ip='" + ip + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guuid guuid1 = (Guuid) o;
        return guuid == guuid1.guuid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(guuid);
    }
}

