import java.util.Arrays;
import java.util.Scanner;

public class ConsoleHelper {

    public static void printLine() {
        System.out.println("----------------------------------------------------------------------");
    }

    public static void print(String... message) {
        System.out.println(convertMessageFromArrayToString(message));
    }

    public static void printError(String... message) {
        System.err.println(convertMessageFromArrayToString(message));
    }

    private static String convertMessageFromArrayToString(String[] message) {
        StringBuilder builder = new StringBuilder();
        Arrays.asList(message).forEach(m -> builder.append(m).append(" "));
        return builder.toString();
    }

    public static void exit() {
        System.exit(1);
    }

    public static int getUserChoiceFromCli(String menuToDisplay)   {
        Scanner scanner = new Scanner(System.in);
        ConsoleHelper.printLine();
        ConsoleHelper.print(menuToDisplay);
        ConsoleHelper.printLine();
        int userInput = 1;
        while (true) {
            try {
                String input = scanner.nextLine();
                if (input.isEmpty())    {
                    continue;
                }
                userInput = Integer.parseInt(input);
            } catch (NumberFormatException ex) {
                ConsoleHelper.print("That input seems undesired. Try again!");
                continue;
            }
            return userInput;
        }
    }
}
