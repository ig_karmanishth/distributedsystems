import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * This node represents a single binary digit in the binary tree which represents
 * the entire routing table of this kademlia peer
 */
public class RoutingTableNode {
    public static final String EMPTY = "-";
    RoutingTableNode left;
    RoutingTableNode right;
    String binary;
    Guuid guuid;

    public RoutingTableNode(@JsonProperty("left") RoutingTableNode left,
                            @JsonProperty("right") RoutingTableNode right,
                            @JsonProperty("binary") String binary) {
        this.left = left;
        this.right = right;
        this.binary = binary;
        if (!binary.equals(EMPTY)) {
            this.guuid = binaryToGuuid(binary);
        }
    }

    public RoutingTableNode(@JsonProperty("left") RoutingTableNode left,
                            @JsonProperty("right") RoutingTableNode right,
                            @JsonProperty("guuid") Guuid guuid) {
        this.left = left;
        this.right = right;
        this.guuid = guuid;
        this.binary = Guuid.getBinaryString(guuid.getGuuid());
    }

    public Guuid getGuuid() {
        return guuid;
    }

    public void setGuuid(Guuid guuid) {
        this.guuid = guuid;
    }

    public String getBinary() {
        return binary;
    }

    private Guuid binaryToGuuid(String binary) {
        try {
            int guuid = Integer.parseInt(binary, 2);
            return new Guuid(guuid, Guuid.getBinaryString(guuid), InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
            ConsoleHelper.exit();
        }
        return null;
    }

    @Override
    public String toString() {
        return "RoutingTableNode{" +
                ", guuid='" + guuid + '\'' +
                ", binary='" + binary + '\'' +
                '}';
    }
}
