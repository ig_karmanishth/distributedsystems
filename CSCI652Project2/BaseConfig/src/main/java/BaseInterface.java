/**
 * The base interface that must be shared by all maven modules namely
 * - Publisher module
 * - Subscriber module
 * - EventManager module
 */
public interface BaseInterface {
    public String getMenuMessage();
    public void startWorking();
}
