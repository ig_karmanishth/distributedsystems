import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Event {
	private int id;
	private Topic topic;
	private String title;
	private String content;
	private Long ttl;

	private Event(Builder builder)	{
		this.id = builder.id;
		this.topic= builder.topic;
		this.title = builder.title;
		this.content = builder.content;
		this.ttl = builder.ttl;
	}

	@JsonCreator
	private Event(@JsonProperty("id") int id,
				  @JsonProperty("topic") Topic topic,
				  @JsonProperty("title") String title,
				  @JsonProperty("content") String content,
				  @JsonProperty("ttl") Long ttl)	{
		this.id = id;
		this.topic = topic;
		this.title = title;
		this.content = content;
		this.ttl = ttl;
	}

	public int getId() {
		return id;
	}

	public Topic getTopic() {
		return topic;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public Long getTtl() {
		return ttl;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Event event = (Event) o;
		return id == event.id &&
				topic.equals(event.topic) &&
				title.equals(event.title) &&
				Objects.equals(content, event.content) &&
				Objects.equals(ttl, event.ttl);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, topic, title, content, ttl);
	}

	@Override
	public String toString() {
		return "Event{" +
				"id=" + id +
				", topic=" + topic +
				", title='" + title + '\'' +
				", content='" + content + '\'' +
				", ttl=" + ttl +
				'}';
	}

	public static class Builder {
		private int id;
		private Topic topic;
		private String title;
		private String content;
		private Long ttl;

		public Builder(Topic topic, String title) {
			this.topic = topic;
			this.title = title;
		}

		public Builder withId(int id)	{
			this.id = id;
			return this;
		}

		public Builder withContent(String content)	{
			this.content = content;
			return this;
		}

		public Builder withTtl(Long ttl)	{
			this.ttl = ttl;
			return this;
		}

		public Builder withTtl(String ttl)	{
			this.ttl = Long.parseLong(ttl);
			return this;
		}

		public Event build()	{
			return new Event(this);
		}
	}
}
