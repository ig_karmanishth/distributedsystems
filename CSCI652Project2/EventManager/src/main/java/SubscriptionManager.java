import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.io.Console;
import java.util.*;

/**
 * A Singleton manager class to handle the current state of mappings between
 * topics, publishers and subscribers.
 *
 * - List<SubscriberModel> subscriberList:  contains all the subscribers (online/offline) that have interacted with the EventManager node so far
 * - Map<Topic, List<SubscriberModel>> topicToSubscriberMap: {  topic1: [sub1, sub2]}
 *                                                              topic2: [sub3, sub4, sub5]
 *                                                           }
 */
public class SubscriptionManager {
    private static final String TAG = SubscriptionManager.class.getSimpleName() + ":";
    private static SubscriptionManager subscriptionManager;
    private List<SubscriberModel> subscriberList;
    private Map<Topic, LinkedHashSet<SubscriberModel>> topicToSubscriberMap;
    // a lockObject to prevent concurrent reading and writing to the topicToSubscriberMap
    private final Object lockObject =  new Object();

    public SubscriptionManager() {
        this.subscriberList = new ArrayList<>();
        this.topicToSubscriberMap = new HashMap<>();
    }

    public static SubscriptionManager getInstance() {
        if (subscriptionManager == null) {
            subscriptionManager = new SubscriptionManager();
        }
        return subscriptionManager;
    }

    public synchronized boolean addSubscriber(SubscriberModel subscriberModel) {
        if (!this.subscriberList.contains(subscriberModel)) {
            this.subscriberList.add(subscriberModel);
        } else {
            // subscriber was already registered, mark as online
            ConsoleHelper.print(TAG, "subscriber already registered, marking online:", subscriberModel.toString());
            this.subscriberList.stream()
                    .filter(s -> s.id == subscriberModel.id)
                    .findFirst()
                    .get()
                    .setActiveStatus(true);
        }
        printSubscriberList();
        return true;
    }

    /**
     * Remove given topic subscription for the given subscriber
     * @param subscriberModel
     * @param topic
     * @return
     */
    public boolean addSubscriptionMapping(SubscriberModel subscriberModel, Topic topic) {
        synchronized (this.lockObject) {
            if (!this.topicToSubscriberMap.containsKey(topic)) {
                // create a new topic with a new list of subscribers
                LinkedHashSet<SubscriberModel> subscriberSet = new LinkedHashSet<>();
                subscriberSet.add(subscriberModel);
                this.topicToSubscriberMap.put(topic, subscriberSet);
            } else {
                // just add the new subscriber to the corresponding collection if not already there
                LinkedHashSet<SubscriberModel> mSet = this.topicToSubscriberMap.get(topic);
                if (mSet.contains(subscriberModel)) {
                    ConsoleHelper.print(TAG, "Subscriber already subscribed to topic! ID: ", Integer.toString(subscriberModel.getId()), ", topic: ", topic.getName());
                } else {
                    mSet.add(subscriberModel);
                }
            }
            printTopicToSubscriberMapping();
            return true;
        }
    }
    // TODO: also make sure that pending notifications are not delivered to the subscriber when they wake up
    /**
     * Remove all subscriptions of the given subscriber
     * @param subscriberModel
     * @return
     */
    public synchronized boolean removeSubscriptionMapping(SubscriberModel subscriberModel) {
        synchronized (this.lockObject) {
            List<Topic> affectedTopics = new ArrayList<>();
            this.topicToSubscriberMap.entrySet().stream()
                    .filter(e -> e.getValue().contains(subscriberModel))
                    // for each topic which contains the given subscriber, remove it
                    .forEach(e -> {
                        affectedTopics.add(e.getKey());
                        e.getValue().remove(subscriberModel);
                    });
            ConsoleHelper.print(TAG, "Subscriber removed from all topics!", Integer.toString(subscriberModel.getId()), " from topics: ", affectedTopics.toString());
            printTopicToSubscriberMapping();
            return true;
        }
    }

    /**
     * Remove one subscription for the given subscriber from the given topic
     * @param subscriberModel
     * @param topic
     * @return
     */
    public synchronized boolean removeSubscriptionMapping(SubscriberModel subscriberModel, Topic topic) {
        synchronized (this.lockObject) {
            if (this.topicToSubscriberMap.containsKey(topic)) {
                // just remove this subscriber from the corresponding collection
                LinkedHashSet<SubscriberModel> mSet = this.topicToSubscriberMap.get(topic);
                if (mSet.contains(subscriberModel)) {
                    mSet.remove(subscriberModel);
                    ConsoleHelper.print(TAG, "Subscriber removed!:", Integer.toString(subscriberModel.getId()), " from topic: ", topic.getName());
                }
            }
            printTopicToSubscriberMapping();
            return true;
        }
    }

    public SubscriberModel getSubscriberFromId(int id) {
        Optional<SubscriberModel> subscriberModel = this.subscriberList.stream()
                .filter(s -> s.getId() == id)
                .findAny();
        if (subscriberModel.isEmpty()) {
            ConsoleHelper.printError(TAG, "No SubscriptionModel found for id:", Integer.toString(id));
            return null;
        }
        return subscriberModel.get();
    }

    public void printSubscriberList() {
        ConsoleHelper.printLine();
        ConsoleHelper.print("UPDATED SUBSCRIBER LIST");
        ConsoleHelper.printLine();
        this.subscriberList.forEach(System.out::println);
        ConsoleHelper.printLine();
    }

    private void printTopicToSubscriberMapping() {
        ConsoleHelper.printLine();
        ConsoleHelper.print("UPDATED SUBSCRIPTION MAP");
        ConsoleHelper.printLine();
        this.topicToSubscriberMap.entrySet().forEach(e -> {
            Topic topic = e.getKey();
            ConsoleHelper.print("Topic:", topic.getName(), "\t:", e.getValue().toString());
        });
        ConsoleHelper.printLine();
    }

    /**
     * Attempt to notify all corresponding subscribers of the new event. We extract the
     * topic from the event and then fire up as many concurrent requests as there are number
     * of subscribers mapped to that topic in this SubscriptionManager.
     */
    public void notifySubscribers(Event event) {
        Topic topicFromEvent = event.getTopic();
        LinkedHashSet<SubscriberModel> subscribersToNotify = topicToSubscriberMap.getOrDefault(topicFromEvent, null);
        if (subscribersToNotify == null || subscribersToNotify.isEmpty()) {
            ConsoleHelper.print(TAG, "Failed! No subscribers found for event!", event.toString(), " with topic:", topicFromEvent.toString());
            return;
        } else {
            // everything looks good, we are ready to notify the subscribers
            ConsoleHelper.print(TAG, "Found", Integer.toString(subscribersToNotify.size()), " subscribers for topic[id, name]:[", Integer.toString(topicFromEvent.getId()), ",", topicFromEvent.getName(), "]");
            for (SubscriberModel subscriber : subscribersToNotify) {
                notify(event, subscriber);
            }
        }
    }

    /**
     * Notify the subscriber of the new event.
     * <p>
     * NOTE: If the subscriber fails to accept the event, it is added to the {@link PendingEventsManager}
     * to be sent some time in the future when the subscriber is back up
     *
     * @param event
     */
    public void notify(Event event, SubscriberModel subscriber) {
        ConsoleHelper.print(TAG, "Attempting to notify subscriber id:", Integer.toString(subscriber.getId()), "about event title:", event.getTitle());
        new Thread(() -> {
            BaseConfig.QosCounter qosCounter = BaseConfig.getInstance().getNewQosCounter();
            while (qosCounter.isRetryAttemptAvailable()) {
                String ip = subscriber.getIp();
                int port = BaseConfig.PORT_SUBSCRIBER_EVENT_NOTIFICATION;
                RpcClient rpcClient = RpcClient.getNew(ip + ":" + port);
                JSONRPC2Response response;
                try {
                    response = rpcClient.sendEventNotification(event);
                    boolean responseSuccess = Boolean.parseBoolean(response.getResult().toString());
                    if (responseSuccess) {
                        // the notification was sent successfully!
                        ConsoleHelper.print(TAG, "success! Notification sent to", subscriber.toString());
                        qosCounter.markSuccessfulTransmission();
                        return;
                    }
                } catch (JSONRPC2SessionException ignored) {
                }
            }
            // something went wrong in communicating with the subscriber
            ConsoleHelper.printError(TAG, "Failed! Cannot communicate with the subscriber", subscriber.toString(), " on port:", Integer.toString(BaseConfig.PORT_SUBSCRIBER_EVENT_NOTIFICATION));
            // add to pending event manager
            PendingEventsManager.getInstance().addPendingEvent(subscriber, event);
        }
        ).start();
    }
}
