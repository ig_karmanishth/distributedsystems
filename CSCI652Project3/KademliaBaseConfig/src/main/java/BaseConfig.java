import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Arrays;
import java.util.List;

public class BaseConfig {
    public static final int MAX_BITS = 4;
    public static final int MINISERVER_CONNECTION_PORT = 9001;
    public static final String MINISERVER_RPC_METHOD_NAME_MARK_ONLINE = "miniserver_rpc_method_mark_online";
    public static final String MINISERVER_RPC_METHOD_NAME_MARK_OFFLINE = "miniserver_rpc_method_mark_offline";
    public static final String MINISERVER_RPC_METHOD_NAME_GET_ONLINE_PEER_GUUID = "miniserver_rpc_method_name_get_online_peer_guuid";
    public static final String MINI_SERVER_IP = "http://172.20.240.21";
    public static final Object RPC_PARAM_HOST = "rpc_param_host";
    public static final String PEER_RPC_METHOD_STORE_FILE = "peer_rpc_method_store_file";
    public static final String PEER_RPC_METHOD_GET_FILE = "peer_rpc_method_get_file";
    public static final String PEER_RPC_METHOD_HOP_HANDLER = "peer_rpc_method_hop_handler";
    public static final int KADEMLIA_PEER_CLIENT_FILE_STORE_PORT = 9002;
    public static final int KADEMLIA_PEER_FILE_STORE_REQUEST_HOP_PORT = 9003;
    public static final int KADEMLIA_PEER_PING_HELLO_PORT = 9004;
    public static final String PEER_RPC_METHOD_HELLO_PING = "rpc_method_hello_ping";
    public static final Guuid anchor_node_1 = new Guuid(2, Guuid.getBinaryString(2),  "172.20.240.40");
    public static final Guuid anchor_node_2 = new Guuid(4, Guuid.getBinaryString(5),  "172.20.240.50");
    public static final int KADEMLIA_PEER_BACKUP_KEY_PORT = 9005;
    public static final String PEER_RPC_METHOD_BACKUP_KEY = "rpc_method_backup_key";
    public static final int MINISERVER_MARK_OFFLINE_PORT = 9006;
    public static final int KADEMLIA_PEER_TRANSFER_PENDING_KEYS_TO_OWNER_PORT = 9007;
    public static final String PEER_RPC_METHOD_RECEIVE_PENDING_KEYS = "rpc_method_receive_pending_keys";
    public static final String RPC_PARAM_CLIENT_IP = "rpc_param_client_name";
    public static final int KADEMLIA_PEER_CLIENT_FILE_GET_PORT = 9008;
    public static final String CLIENT_RPC_METHOD_HANDLE_FILE = "client_rpc_method_handle_get_file";
    public static final String RPC_PARAM_FILE_ORIGIN_PEER = "rpc_param_file_origin_peer";
    public static final int CLIENT_FILE_RECEPTION_PORT = 9010;
    public static List<Guuid> ANCHOR_NODES = Arrays.asList(anchor_node_1, anchor_node_2);

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum ResponseEnum {
        SUCCESS(true),
        FAILURE(false);
        private boolean resultValue;
        ResponseEnum(boolean value) {
            this.resultValue = value;
        }
        public boolean getValue() {
            return resultValue;
        }
    }
}
