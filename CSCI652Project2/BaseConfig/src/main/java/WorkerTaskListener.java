import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

import java.io.IOException;

public interface WorkerTaskListener {
    public RequestHandler getRequestHandler();
    public void onSuccess(JSONRPC2Response object) throws IOException;
}
