import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.Dispatcher;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

import java.io.*;
import java.net.Socket;

/**
 * A singleton worker thread manager that contains all the worker
 * thread classes operational on the subscriber server
 *
 * These classes are namely:
 * - EventWorker
 */
public class RpcServerManager {
    private static RpcServerManager rpcServerManager;
    public static RpcServerManager getInstance()    {
        if (rpcServerManager == null)   {
            rpcServerManager = new RpcServerManager();
        }
        return rpcServerManager;
    }

    /**
     * Return a new worker to handle new events
     * @param socket    to which the worker thread will bind
     * @return
     */
    public EventWorker getNewEventWorker(Socket socket) {
        return new EventWorker(socket);
    }
}

/**
 * A worker thread to listen to incoming events on a
 * dedicated socket
 */
class EventWorker extends Thread implements WorkerTaskListener {
    private static final String TAG = EventWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public EventWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(getRequestHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }
            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            // do not in.close();
            out.flush();
            out.close();
            socket.close();
            this.onSuccess(response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void onSuccess(JSONRPC2Response ignore) {
    }

    @Override
    public RequestHandler getRequestHandler() {
        return RequestHandlingManager.getInstance().getEventRequestHandler();
    }
}