import javax.print.attribute.standard.NumberUp;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.Scanner;

import static java.lang.Thread.sleep;

public class EventManager implements EventManagerInterface, BaseInterface {
	private static String TAG = EventManager.class.getSimpleName();
	RpcServerWorkerThreadManager rpcServerWorkerThreadManager = RpcServerWorkerThreadManager.getInstance();

	public static void main(String[] args) {
		new EventManager().startWorking();
	}

	@Override
	public void startWorking() {
		startAllServices();
	}

	@Override
	public void startAllServices() {
		// start listening to topic advertisements
		startTopicListeningService();
		// start listening to topic get requests
		startTopicSupplierService();
		// start listening to subscribers logins
		startSubscriberLoginService();
		// start listening to subscription requests
		startSubscriptionListeningService();
		// start listening to unsubscription requests
		startUnsubscriptionListeningService();
		// start listening to events being published
		startEventListeningService();
		// start pending notification service
		startPendingNotificationService();

		// start accepting user input from console
		try {
			// allow all the services to fire up before showing the menu
			sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		startMenuService();
	}

	/**
	 * a service that received new topics from publishers and
	 * stores them in memory
	 */
	private void startTopicListeningService() {
		new Thread(() -> {
			try (ServerSocket listener = new ServerSocket(BaseConfig.PORT_EVENT_MANAGER_TOPIC_ADVERTISEMENT)) {
				ConsoleHelper.print(TAG, "service started!", "TopicListeningService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
				while (true) {
					rpcServerWorkerThreadManager.getSaveAdvertisedTopicWorker(listener.accept()).start();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}).start();
	}

    /**
     * Service to return the current in-memory list of
     * topics that the EventManager maintains
     */
    private void startTopicSupplierService() {
		new Thread(() -> {
			try (ServerSocket listener = new ServerSocket(BaseConfig.PORT_EVENT_MANAGER_GET_TOPICS)) {
				ConsoleHelper.print(TAG, "service started!", "TopicSupplierService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
				while (true) {
					rpcServerWorkerThreadManager.getNewTopicSupplierWorker(listener.accept()).start();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}).start();
    }

    /**
     * Service to return pending events back to the subscribers who may
	 * have been unavailable to receive an event previously
     */
    private void startPendingNotificationService() {
		new Thread(() -> {
			try (ServerSocket listener = new ServerSocket(BaseConfig.PORT_EVENT_MANAGER_PENDING_NOTIFICATIONS)) {
				ConsoleHelper.print(TAG, "service started!", "PendingNotificationService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
				while (true) {
					rpcServerWorkerThreadManager.getNewPendingNotificationsWorker(listener.accept()).start();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}).start();
    }

	/**
	 * Service that allows subscribers to login to the system
	 */
	private void startSubscriberLoginService() {
		new Thread(() ->	{
			try (ServerSocket listener = new ServerSocket(BaseConfig.PORT_EVENT_MANAGER_SUBSCRIBER_LOGIN))	{
				ConsoleHelper.print(TAG, "service started!", "LoginService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
				while (true) {
					rpcServerWorkerThreadManager.getNewLoginWorker(listener.accept()).start();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}).start();
	}

	/**
	 * Service that listens to subcsriber subscriptions*
	 */
	private void startSubscriptionListeningService() {
		new Thread(() ->	{
		try (ServerSocket listener = new ServerSocket(BaseConfig.PORT_EVENT_MANAGER_SUBSCRIPTION_LISTENING))	{
			ConsoleHelper.print(TAG, "service started!", "SubscriptionService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
			while (true) {
				rpcServerWorkerThreadManager.getNewSubscriptionWorker(listener.accept()).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		}).start();
	}

	/**
	 * Service that listens to un-subscription requests events from subscribers
	 */
	private void startUnsubscriptionListeningService() {
		new Thread(() ->	{
		try (ServerSocket listener = new ServerSocket(BaseConfig.PORT_EVENT_MANAGER_UNSUBSCRIPTION_LISTENING))	{
			ConsoleHelper.print(TAG, "service started!", "UnsubscriptionListeningService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
			while (true) {
				rpcServerWorkerThreadManager.getNewUnsubscriptionWorker(listener.accept()).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		}).start();
	}

	/**
	 * Service that listens to events from publishers
	 */
	private void startEventListeningService() {
		new Thread(() ->	{
			try (ServerSocket listener = new ServerSocket(BaseConfig.PORT_EVENT_MANAGER_EVENT_PUBLISH))	{
				ConsoleHelper.print(TAG, "service started!", "EventService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
				while (true) {
					rpcServerWorkerThreadManager.getNewEventWorker(listener.accept()).start();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}).start();
	}

	private void startMenuService()	{
		while (true) {
			ConsoleHelper.printLine();
			int userChoice = ConsoleHelper.getUserChoiceFromCli(getMenuMessage());
			ConsoleHelper.printLine();
			switch (userChoice) {
				case 1:
					// take new qos from the user and update the shared base configuration
					int qosChoiceInput = 1;
					while (true) {
						qosChoiceInput = ConsoleHelper.getUserChoiceFromCli(getQosMessage());
						// validate qos
						if (qosChoiceInput >= 0 && qosChoiceInput <= 2) {
							break;
						}
						ConsoleHelper.print(TAG, "Invalid input! A valid qos is in the range [0,2]. Try again.");
					}
					updateQos(qosChoiceInput);
					break;
				case 2:
					// show list of all subscribers
					SubscriptionManager.getInstance().printSubscriberList();
					break;
				case 3:
					// show list of all topics
					TopicManager.getInstance().printTopicList();
					break;
				default:
					break;
			}
		}
	}

	/**
	 * Update the new Qos in the shared Base Configuration
	 * @param selectedQos
	 */
	private void updateQos(int selectedQos) {
		BaseConfig.qos = selectedQos;
	}

	@Override
	public void notifySubscribers(Event event) {

	}

	@Override
	public void addTopic(Topic topic) {

	}

	@Override
	public void addSubscriber() {

	}

	@Override
	public void removeSubscriber() {

	}

	@Override
	public void showSubscribers(Topic topic) {

	}

	@Override
	public String getMenuMessage() {
		return "NOTE: Event Manager: A running log of events will always be sent to the console.\nWhat else would you like to do?\nNOTE: Select an option and press RETURN any time to take an action\n" +
				"1. Update Qos for events [current qos:" + BaseConfig.qos + "]\n" +
				"2. Get list of subscribers\n" +
				"3. Get list of topics\n";
	}

	public String getQosMessage() {
		return "Select a Quality of Service for the system:\n" +
				"0. At most once\n" +
				"1. Exactly once\n" +
				"2. At least once\n";
	}

}
