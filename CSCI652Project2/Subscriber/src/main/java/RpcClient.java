//The Client sessions package

//The Base package for representing JSON-RPC 2.0 messages

//The JSON Smart package for JSON encoding/decoding (optional)

//For creating URLs

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RpcClient {
	private static final String TAG = RpcClient.class.getSimpleName();
	private JSONRPC2Session mySession;

	/**
	 * Singleton instance retriever
	 * @return
	 */
	public static RpcClient getNew(String serverURL)	{
		return new RpcClient().startSession(serverURL);
	}

	/**
	 * Start a client session to the given server IP address
	 */
	public RpcClient startSession(String serverURL) {
		URL parsedUrl = null;
		try {
			parsedUrl = new URL(serverURL);
		} catch (MalformedURLException e) {
			ConsoleHelper.exit();
		}
		mySession = new JSONRPC2Session(parsedUrl);
		return this;
	}

	public List<Topic> getTopics()	{
		return getTopics(BaseConfig.RPC_METHOD_NAME_GET_TOPICS);
	}

	public List<Topic> getMyTopics()	{
		return getTopics(BaseConfig.RPC_METHOD_NAME_GET_MY_TOPICS);
	}

	private List<Topic> getTopics(String methodName)	{
		JSONRPC2Request request = new JSONRPC2Request(methodName, IdentityManager.getInstance().getUniqueHostId());
		JSONRPC2Response response = null;
		try {
			response = mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
		}
		List<Topic> topicList = null;
		try {
			topicList = new ObjectMapper().readValue(response.getResult().toString(), new TypeReference<List<Topic>>(){});
		} catch (IOException e) {
			ConsoleHelper.printError(TAG, ": Error parsing retrieved list of topic");
			e.printStackTrace();
		}
		return topicList;
	}

	public JSONRPC2Response subscribe(Topic topic)	{
		String methodName = BaseConfig.RPC_METHOD_NAME_SUBSCRIBE_TOPIC;
		JSONRPC2Request request = new JSONRPC2Request(methodName, IdentityManager.getInstance().getUniqueHostId());
		Map<String, Object> myParams = new HashMap<>();
		myParams.put(BaseConfig.RPC_PARAM_TOPIC, topic);
		request.setNamedParams(myParams);
		JSONRPC2Response response = null;
		try {
			response = mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * Un-subscribe from a single topic. If success, then this subscriber will stop
	 * listening to any events related to the given topic.
	 * @param topic
	 * @return
	 */
	public JSONRPC2Response unsubscribe(Topic topic)	{
		String methodName = BaseConfig.RPC_METHOD_NAME_UNSUBSCRIBE_TOPIC;
		JSONRPC2Request request = new JSONRPC2Request(methodName, IdentityManager.getInstance().getUniqueHostId());
		Map<String, Object> myParams = new HashMap<String, Object>();
		myParams.put(BaseConfig.RPC_PARAM_TOPIC, topic);
		request.setNamedParams(myParams);
		JSONRPC2Response response = null;
		try {
			response = mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * Un-subscribe from all topics. If success, then this subscriber will stop
	 * listening to all events related to all topics
	 * @return
	 */
	public JSONRPC2Response unsubscribeAll()	{
		String methodName = BaseConfig.RPC_METHOD_NAME_UNSUBSCRIBE_ALL;
		JSONRPC2Request request = new JSONRPC2Request(methodName, IdentityManager.getInstance().getUniqueHostId());
		JSONRPC2Response response = null;
		try {
			response = mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
		}
		return response;
	}

	public JSONRPC2Response subscribeToKeyword(String keyword)	{
		String methodName = BaseConfig.RPC_METHOD_NAME_SUBSCRIBE_KEYWORD;
		JSONRPC2Request request = new JSONRPC2Request(methodName, IdentityManager.getInstance().getUniqueHostId());
		Map<String, Object> myParams = new HashMap<String, Object>();
		myParams.put(BaseConfig.RPC_PARAM_KEYWORD, keyword);
		request.setNamedParams(myParams);
		JSONRPC2Response response = null;
		try {
			response = mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * Attempt to log into the EventManager by supplying a unique id and host ip address
	 *
	 * NOTE: the unique host id in this case is the JsonRpc request id as well
	 * @return
	 * @throws UnknownHostException
	 */
	public JSONRPC2Response login(int uniqueHostId) throws UnknownHostException {
		String methodName = BaseConfig.RPC_METHOD_NAME_LOGIN;
		JSONRPC2Request request = new JSONRPC2Request(methodName, uniqueHostId);
		Map<String, Object> myParams = new HashMap<String, Object>();
		myParams.put(BaseConfig.RPC_PARAM_HOST_IP, "http://" + InetAddress.getLocalHost().getHostAddress());
		request.setNamedParams(myParams);
		JSONRPC2Response response = null;
		try {
			response = mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * Retrieve pending notifications from the Event Manager
	 * @return	List<Event>
	 */
	public List<Event> getPendingNotifications() {
		String methodName = BaseConfig.RPC_METHOD_NAME_EVENT_NOTIFICATION;
		JSONRPC2Request request = new JSONRPC2Request(methodName, IdentityManager.getInstance().getUniqueHostId());
		JSONRPC2Response response = null;
		try {
			response = mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
		}
		List<Event> eventList = new ArrayList<>();
		try {
			eventList = new ObjectMapper().readValue(response.getResult().toString(), new TypeReference<List<Event>>(){});
		} catch (IOException ignored) {
			// there is no valid list of notifications available
		}
		return eventList;
	}
}