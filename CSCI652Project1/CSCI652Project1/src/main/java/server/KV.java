package server;

/**
 * A model class inspired from @link{https://https://gitlab.com/ig_karmanishth/distributedsystems/tree/master/basic_word_count}
 *
 * author: Ishan Guliani (@ig_karmanishth); https://ishanguliani.com
 *
 * version: 1.0
 */
public class KV<K, V> {
    private K key;
    private V value;

    public KV(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}


