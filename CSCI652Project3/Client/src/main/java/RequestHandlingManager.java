import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

/**
 * A collection of all request handler supported by the MiniServer
 */
public class RequestHandlingManager {

    private static RequestHandlingManager requestHandlingManager;

    public static RequestHandlingManager getInstance()   {
        if (requestHandlingManager == null)    {
            requestHandlingManager = new RequestHandlingManager();
        }
        return requestHandlingManager;
    }

    public RequestHandler getFileResponseHandler() {
        return new FileResponseHandler();
    }
}

/**
 * handle the incoming file
 */
class FileResponseHandler implements RequestHandler {

    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.CLIENT_RPC_METHOD_HANDLE_FILE};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        int fileHash = Integer.parseInt(request.getID().toString());
        Guuid originPeer = Guuid.fromObject(request.getNamedParams().get(BaseConfig.RPC_PARAM_FILE_ORIGIN_PEER).toString());
        ConsoleHelper.print("origin peer: " + originPeer);
        if (originPeer == null)   {
            ConsoleHelper.printError("origin peer is null. Something is not right");
            return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE.getValue(), request.getID());
        }
        ConsoleHelper.print("The file: ", Integer.toString(fileHash), " is found at peer: " + originPeer);
        return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
    }
}