import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.Dispatcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * A singleton worker thread manager that contains all the worker
 * thread classes operational on the EventManager server
 *
 * These classes are namely:
 * - SaveNewTopicWorker
 * - TopicSupplierWorker
 */
public class RpcServerWorkerThreadManager {
    private static RpcServerWorkerThreadManager rpcServerWorkerThreadManager;
    public static RpcServerWorkerThreadManager getInstance()    {
        if (rpcServerWorkerThreadManager == null)   {
            rpcServerWorkerThreadManager = new RpcServerWorkerThreadManager();
        }
        return rpcServerWorkerThreadManager;
    }

    /**
     * Get a worker that returns a stored file from the p2p system
     * @param socket
     * @return
     */
    public FileResponseWorker getFileResponseWorker(Socket socket) {
        return new FileResponseWorker(socket);
    }
}

/**
 * A worker thread to return a file from the p2p network
 */
class FileResponseWorker extends Thread  {
    private static final String TAG = FileResponseWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public FileResponseWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(RequestHandlingManager.getInstance().getFileResponseHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            out.flush();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }
}