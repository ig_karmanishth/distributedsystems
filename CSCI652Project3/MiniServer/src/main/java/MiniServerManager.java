import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

public class MiniServerManager {
    private static final String TAG = MiniServerManager.class.getSimpleName();
    RpcServerWorkerThreadManager rpcServerWorkerThreadManager = RpcServerWorkerThreadManager.getInstance();

    public static void main(String[] args) {
        new MiniServerManager().startService();
    }

    private void startService() {
        startNodeStatusManagementService();
        startOnlinePeerSupplierWorker();
    }

    /**
     * Service that saves and updates online/offline statutes for GUUIDs across the p2p system
     */
    private void startNodeStatusManagementService() {
        new Thread(() ->	{
            try (ServerSocket listener = new ServerSocket(BaseConfig.MINISERVER_MARK_OFFLINE_PORT))	{
                ConsoleHelper.print(TAG, "service started successfully!", "NodeStatusManagementService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    rpcServerWorkerThreadManager.getNodeStatusUpdateWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * Service to return a list of all online nodes
     */
    private void startOnlinePeerSupplierWorker() {
        new Thread(() ->	{
            try (ServerSocket listener = new ServerSocket(BaseConfig.MINISERVER_CONNECTION_PORT))	{
                ConsoleHelper.print(TAG, "service started successfully!", "OnlinePeerSupplierWorker on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    rpcServerWorkerThreadManager.getOnlinePeerGuuidSupplierWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
