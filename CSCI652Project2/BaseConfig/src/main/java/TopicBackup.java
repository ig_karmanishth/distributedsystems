
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;

public class TopicBackup {
	private int id;
	private List<String> keywords;
	private String name;

	public TopicBackup() {
	}

	public TopicBackup(int id, List<String> keywords, String name) {
		this.id = id;
		this.keywords = keywords;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "TopicBackup{" +
				"id=" + id +
				", keywords=" + keywords +
				", name='" + name + '\'' +
				'}';
	}
}
