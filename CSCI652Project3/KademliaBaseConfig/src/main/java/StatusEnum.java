import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum StatusEnum {
    ONLINE(true),
    OFFLINE(false);
    private boolean resultValue;
    StatusEnum(boolean value) {
        this.resultValue = value;
    }
    public boolean getValue() {
        return resultValue;
    }
}
