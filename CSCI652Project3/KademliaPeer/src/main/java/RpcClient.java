import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class RpcClient {
	private static final String TAG = RpcClient.class.getSimpleName();
	private JSONRPC2Session mySession;

	/**
	 * Start a client session to the given server IP address
	 */
	public RpcClient(String serverURL) {
		URL parsedUrl = null;
		try {
			parsedUrl = new URL(serverURL);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			ConsoleHelper.exit();
		}
		mySession = new JSONRPC2Session(parsedUrl);
	}

	/**
	 * Fetch a list of all online peer guuids from the miniserver
	 * @return
	 * @throws JSONRPC2SessionException
	 */
	public JSONRPC2Response getOnlinePeerGuuid() throws JSONRPC2SessionException {
		String methodName = BaseConfig.MINISERVER_RPC_METHOD_NAME_GET_ONLINE_PEER_GUUID;
		ConsoleHelper.print("sending request on method name: ", methodName);
		try {
			String serializedJsonString = new ObjectMapper().writeValueAsString(RoutingTable.getInstance().getmGuuid());
			ConsoleHelper.print("sending my Guuid: ", serializedJsonString);
			JSONRPC2Request request = new JSONRPC2Request(methodName, serializedJsonString);
			return mySession.send(request);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			ConsoleHelper.printError("seriazation of the id failed");
		}
		return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE);
	}

	public JSONRPC2Response markOffline() {
		String methodName = BaseConfig.MINISERVER_RPC_METHOD_NAME_MARK_OFFLINE;
		try {
			String myGuuidAsString = new ObjectMapper().writeValueAsString(RoutingTable.getInstance().getmGuuid());
			ConsoleHelper.print("attempting to mark myself as offline: " + myGuuidAsString);
			JSONRPC2Request request = new JSONRPC2Request(methodName, myGuuidAsString);
			return mySession.send(request);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
			ConsoleHelper.printError("problem marking offline!");
		}
		return null;
	}

	public JSONRPC2Response hopToNextPeerToStoreFile(Integer key) throws JSONRPC2SessionException {
		String methodName = BaseConfig.PEER_RPC_METHOD_HOP_HANDLER;
		JSONRPC2Request request = new JSONRPC2Request(methodName, key);
		return mySession.send(request);
	}

	public JSONRPC2Response hopToNextPeerToGetFile(Integer key, String clientIp) throws JSONRPC2SessionException {
		String methodName = BaseConfig.PEER_RPC_METHOD_GET_FILE;
		JSONRPC2Request request = new JSONRPC2Request(methodName, key);
		Map<String, Object> paramMap = new HashMap<>();
		ConsoleHelper.print("sending client host address: " + clientIp);
		paramMap.put(BaseConfig.RPC_PARAM_CLIENT_IP, clientIp);
		request.setNamedParams(paramMap);
		try	{
			return mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
			return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE);
		}
	}

	public JSONRPC2Response transferToOwner(Integer key) throws JSONRPC2SessionException {
		String methodName = BaseConfig.PEER_RPC_METHOD_RECEIVE_PENDING_KEYS;
		JSONRPC2Request request = new JSONRPC2Request(methodName, key);
		return mySession.send(request);
	}

	public JSONRPC2Response sayHello() {
		String methodName = BaseConfig.PEER_RPC_METHOD_HELLO_PING;
		JSONRPC2Request request;
		try {
			ConsoleHelper.print("saying hello to : ", mySession.getURL().toString());
			request = new JSONRPC2Request(methodName, new ObjectMapper().writeValueAsString(RoutingTable.getInstance().getmGuuid()));
			return mySession.send(request);
		} catch (JsonProcessingException | JSONRPC2SessionException e) {
			e.printStackTrace();
			ConsoleHelper.printError("cannot send hello request");
		}
		return null;
	}

	public JSONRPC2Response backupOnNextPeer(Integer key) {
		String methodName = BaseConfig.PEER_RPC_METHOD_BACKUP_KEY;
		JSONRPC2Request request = new JSONRPC2Request(methodName, key);
		try {
			return mySession.send(request);
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
			ConsoleHelper.printError("problem backing up key: " + key);
		}
		return null;
	}

	public JSONRPC2Response sendFileToClient(Integer key) {
		String methodName = BaseConfig.CLIENT_RPC_METHOD_HANDLE_FILE;
		JSONRPC2Request request = new JSONRPC2Request(methodName, key);
		Map<String, Object> paramMap = new HashMap<>();
		try {
			paramMap.put(BaseConfig.RPC_PARAM_FILE_ORIGIN_PEER, new ObjectMapper().writeValueAsString(RoutingTable.getInstance().getmGuuid()));
			request.setNamedParams(paramMap);
			return mySession.send(request);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (JSONRPC2SessionException e) {
			e.printStackTrace();
			ConsoleHelper.printError("failed to send back file to the client from the peer: ");
		}
		return null;
	}
}