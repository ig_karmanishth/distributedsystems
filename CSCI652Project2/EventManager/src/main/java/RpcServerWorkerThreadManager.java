import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.Dispatcher;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * A singleton worker thread manager that contains all the worker
 * thread classes operational on the EventManager server
 *
 * These classes are namely:
 * - SaveNewTopicWorker
 * - TopicSupplierWorker
 */
public class RpcServerWorkerThreadManager {
    private static RpcServerWorkerThreadManager rpcServerWorkerThreadManager;
    public static RpcServerWorkerThreadManager getInstance()    {
        if (rpcServerWorkerThreadManager == null)   {
            rpcServerWorkerThreadManager = new RpcServerWorkerThreadManager();
        }
        return rpcServerWorkerThreadManager;
    }

    /**
     * Return a new worker that returns a list of all topics
     * @param socket    to which the worker thread will bind
     * @return
     */
    public SaveAdvertisedTopicWorker getSaveAdvertisedTopicWorker(Socket socket) {
        return new SaveAdvertisedTopicWorker(socket);
    }

    /**
     * Return a new worker to handle new incoming topic
     * @param socket    to which the worker thread will bind
     * @return
     */
    public TopicSupplierWorker getNewTopicSupplierWorker(Socket socket) {
        return new TopicSupplierWorker(socket);
    }

    /**
     * Return a new worker to handle new subscriber logins
     * @param socket    to which the worker thread will bind
     * @return
     */
    public LoginWorker getNewLoginWorker(Socket socket) {
        return new LoginWorker(socket);
    }

    /**
     * Return a new worker to handle new subscriptions
     * @param socket    to which the worker thread will bind
     * @return
     */
    public SubscriptionWorker getNewSubscriptionWorker(Socket socket) {
        return new SubscriptionWorker(socket);
    }

    /**
     * Return a new worker to handle un-subscription requests
     * @param socket    to which the worker thread will bind
     * @return
     */
    public UnsubscriptionWorker getNewUnsubscriptionWorker(Socket socket) {
        return new UnsubscriptionWorker(socket);
    }

    /**
     * Return a new worker to handle new events
     * @param socket    to which the worker thread will bind
     * @return
     */
    public EventWorker getNewEventWorker(Socket socket) {
        return new EventWorker(socket);
    }

    /**
     * Return a new worker to respond to pending notification requests
     * from subsribers who just came online and want to check if there are
     * any pending notifications for them
     * @param socket    to which the worker thread will bind
     * @return
     */
    public PendingNotificationsWorker getNewPendingNotificationsWorker(Socket socket) {
        return new PendingNotificationsWorker(socket);
    }

}

/**
 * A worker thread responsible for extraction new topics from
 * incoming requests and storing them in the TopicManager
 */
class SaveAdvertisedTopicWorker extends Thread implements WorkerTaskListener {
    private static final String TAG = SaveAdvertisedTopicWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public SaveAdvertisedTopicWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(getRequestHandler());
    }

    /**
     * Services this thread's client by repeatedly requesting a
     * screen name until a unique one has been submitted, then
     * acknowledges the name and registers the output stream for
     * the client in a global set, then repeatedly gets inputs and
     * broadcasts them.
     */
    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            this.onSuccess(response);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            String returnMessage = "Topic registered successfully! for publisher: " + request.getID().toString();
            out.write(new JSONRPC2Response(returnMessage, request.getID()).toJSONString());
            // do not in.close();
            out.flush();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public RequestHandler getRequestHandler() {
        return RequestHandlingManager.getInstance().getTopicAdvertisementHandler();
    }

    @Override
    public void onSuccess(JSONRPC2Response response) throws IOException {
        ConsoleHelper.print(TAG, "attempting to save new topic");
        Object resultTopicObject = response.getResult();
        Topic topic = Topic.fromObject(resultTopicObject);
        TopicManager.getInstance().addTopic(topic);
        ConsoleHelper.print(TAG, ": Added new topic: ", topic.toString());
        // add the topic to the event manager
    }
}

/**
 * A worker thread to return a list of topics stored within the
 * TopicManager and returning them back to the calling socket
 */
class TopicSupplierWorker extends Thread implements WorkerTaskListener {
    private static final String TAG = TopicSupplierWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public TopicSupplierWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(getRequestHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            // do not in.close();
            out.flush();
            out.close();
            socket.close();
            this.onSuccess(response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void onSuccess(JSONRPC2Response response) {
        // write back the list of topics to the manager
        ConsoleHelper.print(TAG, ": Returned latest topic list to host with id:", response.getID().toString());
    }

    @Override
    public RequestHandler getRequestHandler() {
        return RequestHandlingManager.getInstance().getTopicSupplierHandler();
    }
}

/**
 * A worker thread to listen to incoming subscription requests on a
 * dedicated socker
 */
class LoginWorker extends Thread implements WorkerTaskListener {
    private static final String TAG = LoginWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public LoginWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(getRequestHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            // do not in.close();
            out.flush();
            out.close();
            socket.close();
            this.onSuccess(response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void onSuccess(JSONRPC2Response response) {
        ConsoleHelper.print(TAG, "success! Logged in!");
    }

    @Override
    public RequestHandler getRequestHandler() {
        return RequestHandlingManager.getInstance().getLoginRequestHandler();
    }
}

/**
 * A worker thread to listen to incoming subscription requests on a
 * dedicated socket
 */
class SubscriptionWorker extends Thread implements WorkerTaskListener {
    private static final String TAG = SubscriptionWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public SubscriptionWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(getRequestHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
//            ConsoleHelper.print(TAG, "response: ", response.toString());
            out.write(response.toJSONString());
            // do not in.close();
            out.flush();
            out.close();
            socket.close();
            this.onSuccess(response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void onSuccess(JSONRPC2Response response) {
        // write back the list of topics to the manager
        ConsoleHelper.print(TAG, "success! New subscription from subscriber id:", response.getID().toString());
    }

    @Override
    public RequestHandler getRequestHandler() {
        return RequestHandlingManager.getInstance().getSubscriptionRequestHandler();
    }
}

/**
 * A worker thread to listen to incoming un-subscription requests on a
 * dedicated socket
 */
class UnsubscriptionWorker extends Thread implements WorkerTaskListener {
    private static final String TAG = UnsubscriptionWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public UnsubscriptionWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(getRequestHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }

            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            // send response
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
//            ConsoleHelper.print(TAG, "response: ", response.toString());
            out.write(response.toJSONString());
            // do not in.close();
            out.flush();
            out.close();
            socket.close();
            this.onSuccess(response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void onSuccess(JSONRPC2Response response) {
        // write back the list of topics to the manager
        ConsoleHelper.print(TAG, "success! Unsubscription complete:", response.getID().toString());
    }

    @Override
    public RequestHandler getRequestHandler() {
        return RequestHandlingManager.getInstance().getUnsubscriptionRequestHandler();
    }
}

/**
 * A worker thread to listen to incoming events on a
 * dedicated socket
 */
class EventWorker extends Thread implements WorkerTaskListener {
    private static final String TAG = EventWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public EventWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(getRequestHandler());
    }

    public void run() {
        try {
            // Create character streams for the socket.
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read request
            String line;
            line = in.readLine();
            StringBuilder raw = new StringBuilder();
            raw.append("" + line);
            boolean isPost = line.startsWith("POST");
            int contentLength = 0;
            while (!(line = in.readLine()).equals("")) {
                raw.append('\n' + line);
                if (isPost) {
                    final String contentHeader = "Content-Length: ";
                    if (line.startsWith(contentHeader)) {
                        contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            if (isPost) {
                int c = 0;
                for (int i = 0; i < contentLength; i++) {
                    c = in.read();
                    body.append((char) c);
                }
            }
            ConsoleHelper.print(body.toString());
            JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
            JSONRPC2Response response = dispatcher.process(request, null);
            out.write("HTTP/1.1 200 OK\r\n");
            out.write("Content-Type: application/json\r\n");
            out.write("\r\n");
            out.write(response.toJSONString());
            // do not in.close();
            out.flush();
            out.close();
            socket.close();
            this.onSuccess(response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONRPC2ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void onSuccess(JSONRPC2Response response) {
        ConsoleHelper.print(TAG, "Published event was received from host id:", response.getID().toString());
    }

    @Override
    public RequestHandler getRequestHandler() {
        return RequestHandlingManager.getInstance().getEventRequestHandler();
    }
}

/**
 * Worker to respond to pending notification requests from subsribers who
 * just came online and want to check if there are any pending notifications for them
 */
class PendingNotificationsWorker extends Thread implements WorkerTaskListener {
    private static final String TAG = PendingNotificationsWorker.class.getSimpleName() + ":";
    private Socket socket;
    private Dispatcher dispatcher;

    public PendingNotificationsWorker(Socket socket) {
        this.socket = socket;
        // create a new request dispatcher
        this.dispatcher = new Dispatcher();
        // register our handler with it
        dispatcher.register(getRequestHandler());
    }

    public void run() {
        BaseConfig.QosCounter qosCounter = BaseConfig.getInstance().getNewQosCounter();
        while (qosCounter.isRetryAttemptAvailable()) {
            try {
                // Create character streams for the socket.
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

                // read request
                String line;
                line = in.readLine();
                StringBuilder raw = new StringBuilder();
                raw.append("" + line);
                boolean isPost = line.startsWith("POST");
                int contentLength = 0;
                while (!(line = in.readLine()).equals("")) {
                    raw.append('\n' + line);
                    if (isPost) {
                        final String contentHeader = "Content-Length: ";
                        if (line.startsWith(contentHeader)) {
                            contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                        }
                    }
                }
                StringBuilder body = new StringBuilder();
                if (isPost) {
                    int c = 0;
                    for (int i = 0; i < contentLength; i++) {
                        c = in.read();
                        body.append((char) c);
                    }
                }

                ConsoleHelper.print(body.toString());
                JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
                JSONRPC2Response response = dispatcher.process(request, null);
                // send response
                out.write("HTTP/1.1 200 OK\r\n");
                out.write("Content-Type: application/json\r\n");
                out.write("\r\n");
                out.write(response.toJSONString());
                // do not in.close();
                out.flush();
                out.close();
                socket.close();
                this.onSuccess(response);
                break;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONRPC2ParseException e) {
                e.printStackTrace();
            }
        }
        try {
            socket.close();
        } catch (IOException ignored) {
        }
    }

    @Override
    public void onSuccess(JSONRPC2Response response) {
        // mark as sent in the PendingEventsManager
        List<Event> sentEvents = (List<Event>) response.getResult();
        if (!sentEvents.isEmpty()) {
            PendingEventsManager.getInstance().markPendingEventsAsSent(Integer.parseInt(response.getID().toString()), sentEvents);
            ConsoleHelper.print(TAG, "Returned pending notifications to host with id:", response.getID().toString(), ", Marked as sent!");
        }
    }

    @Override
    public RequestHandler getRequestHandler() {
        return RequestHandlingManager.getInstance().getPendingNotificationsRequestHandler();
    }
}