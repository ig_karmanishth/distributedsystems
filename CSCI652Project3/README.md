# Summary
Implemented kademlia protocol for 2^16 peers to store and retrieve hashes of files in log(n) time. Decentralized file replication using sha256 supporting independent peer nodes that can enter/exit the system asynchronously.

Tools/Knowledge areas: docker, docker-compose, java8, socket programming, tcp, udp, multi-threading, thread-pools, asyn programming

# System Design Specifications

## Peer node overview
![](https://www.lucidchart.com/publicSegments/view/ba29d36b-4001-4b73-af61-60ef645db327/image.png)

## Threads and port mappings
![](https://www.lucidchart.com/publicSegments/view/cb0051d4-4afe-4354-b3ef-f3d42a5f6a1d/image.png)

# About
 ```
author:         Ishan Guliani (ig_karmanishth)
Components:     1 network (i.e csci652network_custom_ig_karmanishth) and 5 services:
        1. mini_server_service - should be up before everyone else
        2. kademlia_peer_service - scalable to 16 peers
        3. kademlia_client_service - scalable to multiple numbers
        4. kademlia_anchor_node_1 - should be the first node to be fired up
        5. kademlia_anchor_node_2 - should be the first node to fire up
Versions:       Docker Desktop 2.2.0.5(43884), Dockerfile v3.7, docker-compose 1.25.4
Docker daemon config(tested on): 3 CPUs, 4 GB Memory, 2.5 GB Swap
 ```

 # Overview
  - The p2p system can be used to store and retrieve files across a network of 16 peers (servers) 
  - The max number of peers in this system in 16 by default which is configurable
  - The order of firing up services should be the following - mini_server_service -> kademlia_anchor_node_1 -> kademlia_anchor_node_2 -> any peers and clients

# Future work
The future scope of this work is to expand this network to store 64 MB chunks of files spread across nodes broken down and distributed by peers and recreated on the clients
 
# Instructions to fire up containers with docker-compose 
- NOTE: `cd` into the current project directory *CSCI652Project3* which contains the maven modules _Client_, _KademliaBaseConfig_, _KademliaPeer_, _MiniServer_, project _docker-compose_ and the _Dockerfile_ 
- NOTE: Be sure that the address space `172.20.240.0/24` is available and that the docker daemon has enough memory and cpu as [mentioned above](#about) 
1. **Fire up the mini server**: <br>`$ docker-compose up --build -d mini_server_service`
2. **Fire up the clients, peers and anchor nodes**: <br>`$ docker-compose up --build -d --scale kademlia_client_service=2 --scale kademlia_peer_service=3`
3. You should now have a total of **8 containers** running which can be seen as follows: <br>`$ docker container ls` ![](https://static.wixstatic.com/media/0a127f_6e2ecab35474463dbb56b5cc50eb7d6b~mv2.png)

### Shell session 1 | MiniServer: 
- **Attach to the mini server** and make sure that maven completes installing the modules: <br>`$ docker attach mini_server_service`
- On successful launch, the mini server will start listening to peers <br>![](https://static.wixstatic.com/media/0a127f_1ceb4d1cf3654b4fa0fce618ef7af90c~mv2.png)
- It will also print a running log of all messages  

### Shell session 2 | Anchor node 1 (also peer1): 
- **Attach to anchor node 1:** <br>`$ docker attach kademlia_anchor_node_1`
- **Give it a known Guuid to start off:** when prompted.

### Shell session 3 | Anchor node 2 (also peer2): 
- **Attach to anchor node 1:** <br>`$ docker attach kademlia_anchor_node_2`
- **Give it a known Guuid to start off:** when prompted.
- You should see updated kademlia routing tables across the network (i.e. other online peers)

### Shell session 4 | Peer 3:
- **Attach to a new peer:** `$ docker attach csci652project3_kademlia_peer_service_1`
- **Give it a known Guuid to start off:** when prompted.
- You should see updated kademlia routing tables across the network (i.e. other online peers)


_NOTE: Continue firing up as many peers as you'd like similarly by scaling the service `csci652project3_kademlia_peer_service_1`_


### Shell session 5 | Client:
- **Attach to a new client:** <br> `$ docker attach csci652project3_kademlia_client_service_1`
- The client can store and retrieve hashes of files to and from the kademlia p2p network. Here is how the initial prompt looks like: <br>![](https://static.wixstatic.com/media/0a127f_4476c399d5db4506b6ecfbe39c01a681~mv2.png)
 
NOTE: In this project you can fire up a total of two clients by default. Feel free to modify the docker-compose file to change this behaviour.

# Common errors
Error:      ```ERROR: Pool overlaps with other one on this address space``` <br>
Solution#1:   Restart the docker daemon <br>
Solution#2:   Run `$ docker network ls` and find the ID of the running instance of network beginning with _csci652network*_. Then remove it with `$ docker network rm <ID>` and try running `$ docker-compose up --build` again <br>
Solution#3:   Update the subnet to a more available subnet in _lines 21, 28, 37 and 52 of docker-compose.yml_