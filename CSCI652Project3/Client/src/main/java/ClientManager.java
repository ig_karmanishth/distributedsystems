import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.Random;
import java.util.Scanner;

/**
 * Client fires up and connect to a peer to attempt a file transfer
 */
public class ClientManager {
    public static Guuid NEAREST_NODE;
    public static void main(String[] args) {
        new ClientManager().start();
    }

    private void start() {
        attemptPeerConnection();
        startListeningToFileResponses();
        startMenuDisplayService();
    }

    private void attemptPeerConnection() {
        ConsoleHelper.getUserChoiceFromCli("press 1 to start");
        int random = new Random().nextInt(2);
        NEAREST_NODE = BaseConfig.ANCHOR_NODES.get(random);
        System.out.println("Connection will be established with the nearest anchor node: " + NEAREST_NODE);
    }

    /**
     * A menu to ask the user what they want to do -
     * For instance -
     * 1. Lookup a node
     * 2. Store a file hash
     * 3. Get a file hash
     *
     * This service just runs on the main thread
     */
    private void startMenuDisplayService() {

        while (true) {
            String menu = "What do you want to do ?\n" +
                    "1. Store a file" +
                    "2. Get a file" +
                    "3. exit()";
            int choice = ConsoleHelper.getUserChoiceFromCli(menu);
            switch (choice) {
                case 1:
                    storeFile();
                    break;
                case 2:
                    getFile();
                    break;
                case 3:
                    ConsoleHelper.exit();
                    break;
                default:
                    startMenuDisplayService();
                    break;
            }
        }
    }

    private void startListeningToFileResponses() {
        new Thread(() ->	{
            try (ServerSocket listener = new ServerSocket(BaseConfig.CLIENT_FILE_RECEPTION_PORT))	{
                ConsoleHelper.print("service started successfully!", "ListeningToFileResponses on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    RpcServerWorkerThreadManager.getInstance().getFileResponseWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void getFile() {
        System.out.println("Please specify the hash of the file you would like to obtain");
        Scanner scanner = new Scanner(System.in);
        String hash = scanner.nextLine();
        String anchorPeer = NEAREST_NODE.getIp() + ":" + BaseConfig.KADEMLIA_PEER_CLIENT_FILE_GET_PORT;
        JSONRPC2Response response = new RpcClient(anchorPeer).getFile(hash);
        if (response.indicatesSuccess())    {
            System.out.println("File retrieval attempted.");
        }
    }

    /**
     * Store the file in the p2p network by transfering it to the nearest anchor node
     */
    private void storeFile() {
        System.out.println("Please specify the file name that you want to store in the p2p network: ");
        Scanner scanner = new Scanner(System.in);
        String filename = scanner.nextLine();
        FileToStore fileToStore = new FileToStore(filename);
        String anchorPeer = NEAREST_NODE.getIp() + ":" + BaseConfig.KADEMLIA_PEER_CLIENT_FILE_STORE_PORT;
        ConsoleHelper.print("attempting to store file in nearest node: " + NEAREST_NODE.getIp());
        JSONRPC2Response response = new RpcClient(anchorPeer).storeFile(fileToStore);
        if (response.indicatesSuccess())    {
            System.out.println("File saved successfully! Hash: " + response.getResult().toString());
        }
    }
}
