import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.*;

/**
 * TODO in interaction diagram - mention login in the menu and a port 9009
 */

/**
 * This manager takes care of all subscription related actions such as -
 * - subscribe to an available topic
 * - un-subscribe from an available topic
 *
 */
public class SubscriberManager {
    public static void main(String[] args) {
        new SubscriberWorker().startWorking();
    }
}

class SubscriberWorker implements BaseInterface, SubscriberInterface  {
    RpcServerManager rpcServerManager = RpcServerManager.getInstance();
    private static final String TAG = SubscriberWorker.class.getSimpleName() + ":";
    private static final String MESSAGE_PRESS_RETURN_FOR_RANDOM_ID = "[press RETURN to use random unique id: ";
    private static final String MESSAGE_PRESS_RETURN_TO_SKIP = "[press RETURN to skip]: ";
    private static final String MESSAGE_OPTIONAL = "[OPTIONAL]";
    private static final String MESSAGE_REQUIRED = "[REQUIRED]";
    private static final Long DEFAULT_TTL = 60000L;

    @Override
    public void startWorking()  {
        try {
            startLoginService();
        } catch (UnknownHostException e) {
            ConsoleHelper.printError(TAG, "failed to start login service");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startMenuWorker()   {
        new Thread(() -> {
            int userInput = 0;
            do {
                ConsoleHelper.print("[", "ID:", Integer.toString(IdentityManager.getInstance().getUniqueHostId()), "]");
                String cliMessageForTheUser = getMenuMessage();
                userInput = ConsoleHelper.getUserChoiceFromCli(cliMessageForTheUser);
                processInput(userInput);
            } while (userInput != 4);
        }).start();
    }

    /**
     * The login process involves the subscriber identifying itself to the event manager
     * by sharing its globally unique ID and IP address so that the event manager can talk
     * back to it when needed some time in the future.
     *
     * NOTE: Since this service is the entrypoint it runs on the main thread and fires up
     * other services when it succeeds.
     */
    private void startLoginService() throws IOException {
        ConsoleHelper.print(getLoginMessage());
        Scanner scanner = new Scanner(System.in);
        String userInput = scanner.nextLine();
        while (!userInput.isEmpty())    {
            try {
                int uniqueId = Integer.parseInt(userInput);
                // set this as the unique id
                IdentityManager.getInstance().setUniqueHostId(uniqueId);
                break;
            } catch(NumberFormatException ignored)  {
                ConsoleHelper.printError(TAG, "That does not seem to be a valid integer! Try again entering an id again or press RETURN to use a default unique identifier:");
                userInput = scanner.nextLine();
            }
        }
        ConsoleHelper.print(TAG, "Attempting to login with id:", Integer.toString(IdentityManager.getInstance().getUniqueHostId()));
        RpcClient rpcClient = RpcClient.getNew(BaseConfig.IP_EVENT_MANAGER + ":" + BaseConfig.PORT_EVENT_MANAGER_SUBSCRIBER_LOGIN);
        JSONRPC2Response response = rpcClient.login(IdentityManager.getInstance().getUniqueHostId());
        boolean responseSuccess =  Boolean.parseBoolean(response.getResult().toString());
        if (responseSuccess)   {
            IdentityManager.getInstance().setLoggedIn(true);
            // login is successful! proceed with startup
            startMenuWorker();
            startPendingNotificationsPullWorker();
            startEventListeningWorker();
        } else  {
            // something went wrong
            ConsoleHelper.printError(TAG, "failed response in login service");
            ConsoleHelper.printError(response.getError().getMessage());
        }
    }

    /**
     * Open connection to a port that listens to incoming events from
     * the event manager meant for this subscriber
     */
    private void startEventListeningWorker() {
        new Thread(() -> {
            try (ServerSocket listener = new ServerSocket(BaseConfig.PORT_SUBSCRIBER_EVENT_NOTIFICATION)) {
                ConsoleHelper.print(TAG, "service started!", "EventService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    rpcServerManager.getNewEventWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * Try to fetch any cached notifications that may be stored at
     * the event manager while this subscriber was offline
     */
    private void startPendingNotificationsPullWorker() {
        new Thread(() -> {
            RpcClient rpcClient = RpcClient.getNew(BaseConfig.IP_EVENT_MANAGER + ":" + BaseConfig.PORT_EVENT_MANAGER_PENDING_NOTIFICATIONS);
            List<Event> pendingNotificationList = rpcClient.getPendingNotifications();
            if (pendingNotificationList.isEmpty())  {
                return;
            }
            ConsoleHelper.print(TAG, "New notifications available:");
            // print the notifications on the console
            pendingNotificationList.forEach(event -> ConsoleHelper.print("[ title:", event.getTitle(), ", content:", event.getContent(), "]"));
        }
        ).start();
    }

    /**
     * Take action on user input based on the menu specified in {@link #getMenuMessage()}
     * @param userInput
     */
    private void processInput(int userInput) {
        switch (userInput)  {
            case 1:
                Topic topic = getTopicFromUser();
                if (topic != null)  {
                    subscribe(topic);
                }
                break;
            case 2:
                handleUnsubscription();
            case 3:
                listSubscribedTopics();
            default:
                break;
        }
    }

    /**
     * Take care of the user's unsubcription request. Here are the two options the
     * user can choose from in case he/she wishes to unsubscribe -
     * 1. un-subscribe from a single topic
     * 2. un-subscribe from all topics that I have subscribed to so far
     */
    private void handleUnsubscription() {
        // ask the user whether to unsubscribe from a single topic or all topics
        int submenuUserInput = 0;
        String cliMessageForTheUser = getUnsubscribeCliPromptMessage();
        while (true) {
            submenuUserInput = ConsoleHelper.getUserChoiceFromCli(cliMessageForTheUser);
            switch (submenuUserInput) {
                case 1:
                    Set<Topic> subscribedTopics = IdentityManager.getInstance().getSubscribedTopicList();
                    Topic topicTounsubscribeFrom = getTopicFromUserPrompt(subscribedTopics);
                    unsubscribe(topicTounsubscribeFrom);
                    break;
                case 2:
                    unsubscribe();
                    break;
                default:
                    continue;
            }
            break;
        }
    }

    /**
     * Prompt the user to select a topic from a list of topics
     * @return  the topic selected by the user
     */
    private Topic getTopicFromUser() {
        RpcClient rpcClient = RpcClient.getNew(BaseConfig.IP_EVENT_MANAGER + ":" + BaseConfig.PORT_EVENT_MANAGER_GET_TOPICS);
        // get topics from the event manager and show to the user to select from
        List<Topic> topics = rpcClient.getTopics();
        if (topics.isEmpty())   {
            ConsoleHelper.print(TAG, "None available!");
            return null;
        }
        return getTopicFromUserPrompt(topics);
    }

    private Topic getTopicFromUserPrompt(Collection<Topic> topicListFromEventManager) {
        Scanner scanner = new Scanner(System.in);
        Topic associatedTopic = null;
        // take topic name
        while (true)    {
            HashMap<Integer, Topic> topicMenuMap = printTopicList(topicListFromEventManager);
            ConsoleHelper.print("Enter the sno to select a topic: ");
            if (!scanner.hasNextInt())  {
                ConsoleHelper.printError("Oops! That seems to be an invalid input. Try again.");
                continue;
            }
            int choice = scanner.nextInt();
            associatedTopic = topicMenuMap.getOrDefault(choice, null);
            if (associatedTopic == null) {
                ConsoleHelper.printError("Oops! That choice does not exist. Try again.");
            } else {
                break;
            }
        }
        return associatedTopic;
    }

    /**
     * Print all available topics by serial number
     * Example: 1. {name: topic1, id:007, keywords:[bitcoin, ether, ripple]}
     *          2. {name: topic2, id:10099, keywords:[zec, ripple]}
     * @return
     */
    private HashMap<Integer, Topic> printTopicList(Collection<Topic> topicListFromEventManager) {
        if (topicListFromEventManager.isEmpty())    {
            ConsoleHelper.print("Sorry! None available");
            return new HashMap<>();
        }
        ConsoleHelper.print("Here are the topics: ");
        HashMap<Integer, Topic> topicMenuMap = new HashMap<>();
        int sno = 1;
        for (Topic topic : topicListFromEventManager)  {
            topicMenuMap.put(sno, topic);
            sno += 1;
        }
        // show the menu to the user
        topicMenuMap.forEach((key, value) -> ConsoleHelper.print(key.toString(), ".", value.toString()));
        return topicMenuMap;
    }

    /**
     * Subscribe to a topic on the event manager
     *
     * NOTE: the receiving port on the Event manager for all subscriptions remains
     * the same regardless of topic based or keyword based subscription
     * @param topicToSubscribeTo
     */
    @Override
    public void subscribe(Topic topicToSubscribeTo) {
        RpcClient rpcClient = RpcClient.getNew(BaseConfig.IP_EVENT_MANAGER + ":" + BaseConfig.PORT_EVENT_MANAGER_SUBSCRIPTION_LISTENING);
        JSONRPC2Response response = rpcClient.subscribe(topicToSubscribeTo);
        boolean responseSuccess =  Boolean.parseBoolean(response.getResult().toString());
        if (responseSuccess)    {
            IdentityManager.getInstance().addTopic(topicToSubscribeTo);
            ConsoleHelper.print(TAG, "success!, subscribed to topic: ", topicToSubscribeTo.toString());
        } else  {
            ConsoleHelper.printError(TAG, "failed! response failed! could not subscribe to topic: ", topicToSubscribeTo.toString());
        }
    }

    /**
     * Subscribe to a keyword on the event manager
     *
     * NOTE: the receiving port on the Event manager for all subscriptions remains
     * the same regardless of topic based or keyword based subscription
     * @param keyword
     */
    @Override
    public void subscribe(String keyword) {
        RpcClient rpcClient = RpcClient.getNew(BaseConfig.IP_EVENT_MANAGER + ":" + BaseConfig.PORT_EVENT_MANAGER_SUBSCRIPTION_LISTENING);
        JSONRPC2Response response = rpcClient.subscribeToKeyword(keyword);
        if (response.getResult() == BaseConfig.ResponseEnum.SUCCESS)    {
            IdentityManager.getInstance().addKeyword(keyword);
            ConsoleHelper.print(TAG, "success!, subscribed to keyword: ", keyword);
        } else  {
            ConsoleHelper.printError(TAG, "failed! response failed! could not subscribe to keyword: ", keyword);
        }
    }

    /**
     * Un-subscribe from topic on the event manager
     * @param topicToUnsubscribeFrom
     */
    @Override
    public void unsubscribe(Topic topicToUnsubscribeFrom) {
        RpcClient rpcClient = RpcClient.getNew(BaseConfig.IP_EVENT_MANAGER + ":" + BaseConfig.PORT_EVENT_MANAGER_UNSUBSCRIPTION_LISTENING);
        JSONRPC2Response response = rpcClient.unsubscribe(topicToUnsubscribeFrom);
        if (response.getResult() == BaseConfig.ResponseEnum.SUCCESS)    {
            IdentityManager.getInstance().removeTopic(topicToUnsubscribeFrom);
            ConsoleHelper.print(TAG, "success!, un-subscribed from topic: ", topicToUnsubscribeFrom.toString());
        } else  {
            ConsoleHelper.printError(TAG, "failed! response failed! could not un-subscribe from topic: ", topicToUnsubscribeFrom.toString());
        }
    }

    @Override
    public void unsubscribe() {
        RpcClient rpcClient = RpcClient.getNew(BaseConfig.IP_EVENT_MANAGER + ":" + BaseConfig.PORT_EVENT_MANAGER_UNSUBSCRIPTION_LISTENING);
        JSONRPC2Response response = rpcClient.unsubscribeAll();
        if (response.getResult() == BaseConfig.ResponseEnum.SUCCESS)    {
            IdentityManager.getInstance().removeAll();
            ConsoleHelper.print(TAG, "success!, un-subscribed from all topics! ");
        } else  {
            ConsoleHelper.printError(TAG, "failed! response failed! could not un-subscribe from all topics!");
        }
    }

    @Override
    public void listSubscribedTopics() {
        printTopicList(IdentityManager.getInstance().getSubscribedTopicList());
    }

    public String getLoginMessage() {
        return "Do you already know your subscriber_id ? If yes please enter it now. Otherwise press RETURN to continue with a default unique ID";
    }

    @Override
    public String getMenuMessage() {
        return "What would you like to do?\n" +
                "1. Subscribe to a topic\n" +
                "2. Unsubscribe from a topic\n" +
                "3. View list of all subscribed topics\n" +
                "4. logout and exit\n";
    }
    /**
     * The message shown to the user when he/she selects the unsubscribe option
     * @return
     */
    private String getUnsubscribeCliPromptMessage() {
        return "What would you like to do?\n" +
                "1. Unsubscribe from singe topic\n" +
                "2. Unsubscribe from all topics\n";
    }
}
