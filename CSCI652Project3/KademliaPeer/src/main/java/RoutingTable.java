import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RoutingTable {
    private static RoutingTable mRoutingTable;
    private Guuid mGuuid;
    RoutingTableNode rootNode;
    List<List<RoutingTableNode>> groupedKBucket = new ArrayList<>();
    List<RoutingTableNode> kBuckets = new ArrayList<>();

    public static RoutingTable getInstance(Guuid guuid)  {
        if (mRoutingTable == null)  {
            mRoutingTable = new RoutingTable(guuid);
        }
        return mRoutingTable;
    }

    public static RoutingTable getInstance()  {
        return mRoutingTable;
    }

    private RoutingTable(@JsonProperty("guuid") Guuid guuid) {
        this.mGuuid = guuid;
    }

    public void refresh() {
        this.kBuckets.clear();
        this.groupedKBucket.clear();
        this.kBuckets.add(new RoutingTableNode(null, null, mGuuid));
        this.groupedKBucket.add(Collections.singletonList(new RoutingTableNode(null, null, mGuuid)));
        initializeEmptyNodes();
//        inorderTraversal(rootNode);
        updateLocalKRegions();
        syncWithMiniServer();
    }

    public Guuid getmGuuid() {
        return mGuuid;
    }

    private void inorderTraversal(RoutingTableNode rootNode) {
        if (rootNode == null)   {
            return;
        }
        inorderTraversal(rootNode.left);
        System.out.println(rootNode.binary);
        inorderTraversal(rootNode.right);
    }

    /**
     * Update the tree such that every node corresponding to a letter
     * in the current GUUID holds the online node
     * steps:
     * go down to the leaf corresponding to the GUUID
     * backtrack one by one and run a DFS on each node
     * now update the list of k regions in each of those nodes to contain the ONLINE node
     * which is closest to the current node XORwise
     */
    private void updateLocalKRegions() {
        List<String> guuidAsStringList = this.mGuuid.asListOfString();
        traverse(rootNode, guuidAsStringList, 0);
    }

    private void traverse(RoutingTableNode rootNode, List<String> guuidAsStringList, int i) {
        if (i == guuidAsStringList.size())   {
            // on the leaf node, time to backtrack
            return;
        }
        int nextLevel = i+1;
        if (guuidAsStringList.get(i).equals("0") && rootNode.left != null)    {
            traverse(rootNode.left, guuidAsStringList, nextLevel);
            List<RoutingTableNode> childNodes = new ArrayList<>();
            getListOfChildNodesHelper(rootNode.right, childNodes);
            if (!childNodes.isEmpty()) {
                this.groupedKBucket.add(childNodes);
            }
        } else if (guuidAsStringList.get(i).equals("1") && rootNode.right != null)  {
            traverse(rootNode.right, guuidAsStringList, nextLevel);
            List<RoutingTableNode> childNodes = new ArrayList<>();
            getListOfChildNodesHelper(rootNode.left, childNodes);
            if (!childNodes.isEmpty()) {
                this.groupedKBucket.add(childNodes);
            }
        }
    }

    private void getListOfChildNodesHelper(RoutingTableNode rootNode, List<RoutingTableNode> childNodes) {
        if (rootNode == null){
            return;
        }
        if (rootNode.left == null && rootNode.right == null){
            childNodes.add(rootNode);
        }
        getListOfChildNodesHelper(rootNode.left, childNodes);
        getListOfChildNodesHelper(rootNode.right, childNodes);
    }

    private void initializeEmptyNodes() {
        rootNode = initializeEmptyNodesHelper(0, BaseConfig.MAX_BITS, "");
    }
    private RoutingTableNode initializeEmptyNodesHelper(int currLevel, int maxLevel, String binaryString)   {
        if (currLevel > maxLevel)   {
            return null;
        }
        String mString = RoutingTableNode.EMPTY;
        if (currLevel == maxLevel)   {
            mString = binaryString;
        }
        RoutingTableNode left = initializeEmptyNodesHelper(currLevel + 1, maxLevel, binaryString + "0");
        RoutingTableNode right = initializeEmptyNodesHelper(currLevel + 1, maxLevel, binaryString + "1");
        return new RoutingTableNode(left, right, mString);
    }

    /**
     * Make a call ro rhe mini server ro fetch the list of online peer Guuids
     */
    private void syncWithMiniServer() {
        ConsoleHelper.print("syncing with mini server...");
        try {
            JSONRPC2Response response = new RpcClient(BaseConfig.MINI_SERVER_IP + ":" + BaseConfig.MINISERVER_CONNECTION_PORT).getOnlinePeerGuuid();
            List<Guuid> onlinePeerGuuidList = new ObjectMapper().readValue(response.getResult().toString(), new TypeReference<List<Guuid>>(){});
//            ConsoleHelper.print("received list: ");
//            onlinePeerGuuidList.forEach(g -> ConsoleHelper.print(g.toString(), "\n"));
//            ConsoleHelper.print("kregions: ", this.kRegions.toString());
//            printGroupedKBuckets();
            filterOutOfflinePeers(onlinePeerGuuidList);
            keepOnlyClosestXorPerBucket();
//            this.kBuckets = this.kBuckets.stream()
//                    .filter(node -> onlinePeerGuuidList.contains(node.getGuuid()))
//                    .peek(node -> onlinePeerGuuidList.stream().forEach(onlinePeer -> {
//                        if (onlinePeer.getGuuid() == node.getGuuid().getGuuid()){
//                            node.setGuuid(onlinePeer);
//                        }
//                    }))
//                    .collect(Collectors.toList());
            ConsoleHelper.print("sync complete! Updated routing table:");
            show();
//            xor(kRegions);
        } catch (JSONRPC2SessionException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void keepOnlyClosestXorPerBucket() {
        List<RoutingTableNode> tempKBucketList = new ArrayList<>();
        ConsoleHelper.print("attempting to xor things out");
        for (List<RoutingTableNode> bucketList : groupedKBucket)    {
            RoutingTableNode nodeWithMinXor = getNodeWithMinXor(bucketList);
            if (nodeWithMinXor != null) {
                tempKBucketList.add(nodeWithMinXor);
            }
        }
        ConsoleHelper.print("temp kbucket list: \n===================");
        tempKBucketList.forEach(bucket -> ConsoleHelper.print(bucket.toString()));
        ConsoleHelper.print("===================");
        this.kBuckets = tempKBucketList;
    }

    private RoutingTableNode getNodeWithMinXor(List<RoutingTableNode> bucketList) {
        ConsoleHelper.print("finding min xor node for list: " + bucketList.toString());
        if (bucketList.isEmpty())   {
            return null;
        }
        int minXor = this.getmGuuid().getGuuid() ^ bucketList.get(0).getGuuid().getGuuid();
        RoutingTableNode nodeWithMinXor = bucketList.get(0);
        for (int i = 1; i < bucketList.size(); i++) {
            RoutingTableNode currentNode = bucketList.get(i);
            int xor = currentNode.getGuuid().getGuuid() ^ this.getmGuuid().getGuuid();
            if (xor < minXor) {
                minXor = xor;
                nodeWithMinXor = currentNode;
            }
        }
        ConsoleHelper.print("returning min xor node: " + nodeWithMinXor);
        return nodeWithMinXor;
    }

    private void printGroupedKBuckets() {
        ConsoleHelper.print("grouped kbuckets: \n===================");
        groupedKBucket.forEach(bucket -> ConsoleHelper.print(bucket.toString()));
        ConsoleHelper.print("===================");
    }

    private void filterOutOfflinePeers(List<Guuid> onlinePeerGuuidList) {
        ConsoleHelper.print("filtering our online peers from grouped k buckets");
        List<List<RoutingTableNode>> list = new ArrayList<>();
        for (List<RoutingTableNode> bucketList : this.groupedKBucket) {
            bucketList = bucketList.stream()
                    .filter(node -> (onlinePeerGuuidList.contains(node.getGuuid())))
                    .peek(node -> onlinePeerGuuidList.forEach(onlinePeer -> {
                        if (onlinePeer.getGuuid() == node.getGuuid().getGuuid()) {
                            node.setGuuid(onlinePeer);
                        }
                    }))
                    .collect(Collectors.toList());
            list.add(bucketList);
        }
        this.groupedKBucket = list;
        ConsoleHelper.print("filtering complete: ");
//        printGroupedKBuckets();
    }

    /**
     * start prefix matching and store the file at the closest peer
     * @param key
     */
    public void attemptToStore(Integer key) {
        ConsoleHelper.print("Received request to store file in p2p network with key: " + key);
        ConsoleHelper.print("finding the next hop: ");
        Guuid closestGuuid = getClosestGuuid(key, false);
        if (closestGuuid == null)   {
            ConsoleHelper.printError("no closest guuid found while attempting to store");
            return;
        }
        if (closestGuuid.equals(this.mGuuid))    {
            ConsoleHelper.print("FOUND! This key belongs to me! Saving");
            StorageManager.getInstance().addKey(key);
        } else {
            hopToAnotherPeerToStoreKey(closestGuuid, key);
        }
    }

    /**
     * start prefix matching and store the file at the closest peer
     * @param key
     */
    public void attemptToGetFile(Integer key, String clientIp) {
        ConsoleHelper.print("Received request to get file in p2p network with key: " + key);
        // check the current storage
        if (StorageManager.getInstance().getStoredKeys().contains(key)) {
            ConsoleHelper.print("FOUND! I have this key! Returning to client: " + clientIp);
            sendKeyBackToClient(key, clientIp);
            return;
        }
        Guuid closestGuuid = getClosestGuuid(key, false);
        if (closestGuuid == null) {
            ConsoleHelper.printError("no closest guuid found while attempting to get file");
        } else if (closestGuuid.equals(getmGuuid()))    {
            // i should have it but I dont, meaning it does not exist
            ConsoleHelper.print("key not found in here!");
        } else {
            hopToAnotherPeerToGetFile(closestGuuid, key, clientIp);
        }
    }

    private void sendKeyBackToClient(Integer key, String clientIp) {
        JSONRPC2Response response;
        String clientAddress = clientIp + ":" + BaseConfig.CLIENT_FILE_RECEPTION_PORT;
        ConsoleHelper.print("found key being sent back to client on: " + clientAddress);
        response = new RpcClient(clientAddress).sendFileToClient(key);
        if (response.indicatesSuccess()) {
            ConsoleHelper.print("file sent back to client successfully!");
        } else {
            ConsoleHelper.printError("file sending to client failed!");
        }
    }

    private void hopToAnotherPeerToStoreKey(Guuid closestGuuid, Integer key) {
        JSONRPC2Response response;
        try {
            String nextHop = closestGuuid.getIp() + ":" + BaseConfig.KADEMLIA_PEER_FILE_STORE_REQUEST_HOP_PORT;
            ConsoleHelper.print("attempting next hop: " + nextHop);
            response = new RpcClient(nextHop).hopToNextPeerToStoreFile(key);
            if (response.indicatesSuccess()) {
                ConsoleHelper.print("file passed on to the next peer successfully!");
            } else {
                ConsoleHelper.printError("file passing error");
            }
        } catch (JSONRPC2SessionException e) {
            e.printStackTrace();
        }
    }

    private void hopToAnotherPeerToGetFile(Guuid closestGuuid, Integer key, String clientIp) {
        JSONRPC2Response response;
        try {
            String nextHop = closestGuuid.getIp() + ":" + BaseConfig.KADEMLIA_PEER_CLIENT_FILE_GET_PORT;
            ConsoleHelper.print("attempting next hop to get file: " + nextHop);
            response = new RpcClient(nextHop).hopToNextPeerToGetFile(key, clientIp);
            if (response.indicatesSuccess()) {
                ConsoleHelper.print("get request passed on to the next peer successfully!");
            } else {
                ConsoleHelper.printError("file passing error");
            }
        } catch (JSONRPC2SessionException e) {
            e.printStackTrace();
        }
    }

    private Guuid getClosestGuuid(Integer key, boolean exceptSelf) {
        int minXor = key ^ this.kBuckets.get(0).getGuuid().getGuuid();
        Guuid closestGuuid = this.kBuckets.get(0).getGuuid();
        int startIndex = 0;
        if (exceptSelf) {
            if (this.kBuckets.size() <= 1)   {
                return null;
            }
            minXor = key ^ this.kBuckets.get(1).getGuuid().getGuuid();
            closestGuuid = this.kBuckets.get(1).getGuuid();
            startIndex = 1;
        }
        for (int i = startIndex + 1; i < this.kBuckets.size(); i++) {
            RoutingTableNode onlineNode = this.kBuckets.get(i);
            int xorDistanceBetweenKeyAndNode = key ^ onlineNode.getGuuid().getGuuid();
            if (xorDistanceBetweenKeyAndNode < minXor)  {
                minXor = xorDistanceBetweenKeyAndNode;
                closestGuuid = onlineNode.getGuuid();
            }
        }
        ConsoleHelper.print("minXor: " + minXor);
        ConsoleHelper.print("closestGuiud: " + closestGuuid.toString());
        return closestGuuid;
    }

    private void show() {
        ConsoleHelper.print("------------------------------------------------------");
        this.kBuckets.forEach(node -> ConsoleHelper.print(node.getBinary(), ": ", node.getGuuid().toString()));
        ConsoleHelper.print("------------------------------------------------------");
    }

    /**
     * say hello to nearby peers
     */
    public synchronized void sayHelloToPeers() {
        this.kBuckets.stream().skip(1).forEach(
                onlineNode -> {
//                    ConsoleHelper.print("attempting to say hi to node: " + onlineNode);
                    if (!onlineNode.getGuuid().equals(getmGuuid())) {
                        JSONRPC2Response response = new RpcClient(onlineNode.getGuuid().getIp() + ":" + BaseConfig.KADEMLIA_PEER_PING_HELLO_PORT).sayHello();
                        if (response.indicatesSuccess()) {
                            ConsoleHelper.print("Just sent a heartbeat to : " + onlineNode.toString());
                        } else {
                            ConsoleHelper.printError("heartbeat sending failed!");
                        }
                    } else  {
                        ConsoleHelper.printError("oops this is myself");
                    }
                }
        );
    }

    /**
     * Distribute all stored keys to the neighbouring peers before safely exiting the system
     */
    public void backupKeysBeforeDying() {
        markSelfAsOffline();
        StorageManager.getInstance().getStoredKeys().forEach(
                storedKey -> {
                    Guuid closestGuuid = getClosestGuuid(storedKey, true);
                    if (closestGuuid == null)   {
                        ConsoleHelper.printError("no closest guuid found while attempting to store key: ", Integer.toString(storedKey));
                    } else {
                        ConsoleHelper.print("found the closest guuid for the stored key: ", Integer.toString(storedKey), " as: " + closestGuuid.toString());
                        hopToAnotherPeerToBackupKey(closestGuuid, storedKey);
                    }
                });
    }

    private void markSelfAsOffline() {
        JSONRPC2Response response;
        String miniServerAddress = BaseConfig.MINI_SERVER_IP + ":" + BaseConfig.MINISERVER_MARK_OFFLINE_PORT;
        ConsoleHelper.print("attempting to hit the server: " + miniServerAddress);
        response = new RpcClient(miniServerAddress).markOffline();
        if (response.indicatesSuccess()) {
            ConsoleHelper.print("marked offline successfully!");
        } else {
            ConsoleHelper.printError("failed! could not mark offline mGuuid: ");
        }
    }

    private void hopToAnotherPeerToBackupKey(Guuid closestGuuid, Integer key) {
        JSONRPC2Response response;
        String nextHop = closestGuuid.getIp() + ":" + BaseConfig.KADEMLIA_PEER_BACKUP_KEY_PORT;
        ConsoleHelper.print("attempting to pass the file to the next peer: " + nextHop);
        response = new RpcClient(nextHop).backupOnNextPeer(key);
        if (response.indicatesSuccess()) {
            ConsoleHelper.print("file passed on to the next peer successfully! Removing from self.");
            StorageManager.getInstance().removeKey(key);
        } else {
            ConsoleHelper.printError("failed! could not pass the key: ", Integer.toString(key));
        }
    }

    /**
     * Go over all the owned keys and give bac keys to online peers if min xor distance
     */
    public void giveBackKeysToNewPeers() {
        StorageManager.getInstance().getStoredKeys().forEach(
                storedKey -> {
                    Guuid closestGuuid = getClosestGuuid(storedKey, false);
                    if (closestGuuid == null)   {
                        ConsoleHelper.printError("no closest guuid found while attempting to store key: ", Integer.toString(storedKey));
                    } else if (closestGuuid.equals(getmGuuid()))    {
                        // do nothing just keep this key
                    }  else {
                        ConsoleHelper.print("found the closest guuid for the stored key: ", Integer.toString(storedKey), " as: " + closestGuuid.toString());
                        transferPendingKeysToRightfulOwner(closestGuuid, storedKey);
                    }
                });
    }

    private void transferPendingKeysToRightfulOwner(Guuid closestGuuid, Integer key) {
        JSONRPC2Response response;
        try {
            String keyOwner = closestGuuid.getIp() + ":" + BaseConfig.KADEMLIA_PEER_TRANSFER_PENDING_KEYS_TO_OWNER_PORT;
            ConsoleHelper.print("actual key owner " + keyOwner);
            response = new RpcClient(keyOwner).transferToOwner(key);
            if (response.indicatesSuccess()) {
                ConsoleHelper.print("pending key: " + key, " transferred to owner: " + keyOwner);
                StorageManager.getInstance().removeKey(key);
            } else {
                ConsoleHelper.printError("could not transfer key: " + key + " to owner: " + keyOwner);
            }
        } catch (JSONRPC2SessionException e) {
            e.printStackTrace();
        }
    }

/*    private void xor(List<List<RoutingTableNode>> kRegions) {
        this.kRegions = kRegions.stream().peek(mList -> {
            if (mList.size() > 0)   {

                this.mGuuid.guuid^
            }
        })
    }*/
}
