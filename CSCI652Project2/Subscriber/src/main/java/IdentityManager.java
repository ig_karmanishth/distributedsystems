import java.time.Instant;
import java.util.*;

/**
 * A singleton identity manager to maintain unique identity of this Subscriber
 * - unique host id
 * - login status
 * - subscription status
 * - topic name
 */
public class IdentityManager {
    private static final String TAG = IdentityManager.class.getSimpleName() + ":";
    private static IdentityManager identityManager;
    private int uniqueHostId;
    private boolean isLoggedIn;
    private Set<Topic> subscribedTopicList;
    private List<String> subscribedKeywordList;

    public IdentityManager(int id) {
        this.uniqueHostId = id;
        subscribedTopicList = new HashSet<>();
        subscribedKeywordList = new ArrayList<>();
        isLoggedIn = false;
    }

    public static IdentityManager getInstance() {
        if (identityManager == null)    {
            identityManager = new IdentityManager(Math.abs(Objects.hash(Instant.now())%10000));
        }
        return identityManager;
    }

    public int getUniqueHostId() {
        return uniqueHostId;
    }

    public void setUniqueHostId(int uniqueHostId) {
        this.uniqueHostId = uniqueHostId;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        ConsoleHelper.print(TAG, "Logged in successfully!");
        isLoggedIn = loggedIn;
    }

    public Set<Topic> getSubscribedTopicList() {
        return subscribedTopicList;
    }

    public boolean addTopic(Topic topic)  {
        this.subscribedTopicList.add(topic);
        return true;
    }

    public boolean removeTopic(Topic topic)  {
        this.subscribedTopicList.remove(topic);
        return true;
    }

    public List<String> getSubscribedKeywordList() {
        return subscribedKeywordList;
    }

    public boolean addKeyword(String keyword)  {
        this.subscribedKeywordList.add(keyword);
        return true;
    }

    public boolean removeAll()  {
        this.subscribedTopicList.clear();
        this.subscribedKeywordList.clear();
        return true;
    }
}
