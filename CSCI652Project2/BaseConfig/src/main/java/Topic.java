import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class Topic {

	private static final String TAG = Topic.class.getSimpleName();
	private int id;
	private List<String> keywords;
	private String name;

	/**
	 * A private constructor enforces use of Builder pattern to get a new Topic
	 * @param builder
	 */
	private Topic(Builder builder) {
		this.id = builder.id;
		this.keywords = builder.keywords;
		this.name = builder.name;
	}

	@JsonCreator
	private Topic(@JsonProperty("id") int id,
				  @JsonProperty("keywords") List<String> keywords,
				  @JsonProperty("name") String name)	{
		this.id = id;
		this.keywords = keywords;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Topic{" +
				"id:" + id +
				", keywords:" + keywords +
				", name:'" + name + '\'' +
				'}';
	}

	public static Topic fromObject(Object object) throws IOException {
		return new ObjectMapper().readValue(object.toString(), Topic.class);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Topic topic = (Topic) o;
		return id == topic.id &&
				Objects.equals(keywords, topic.keywords) &&
				name.equals(topic.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, keywords, name);
	}

	/**
	 * Builder pattern guarantees that new Topic is created safely and
	 * with the correct requirements.
	 * Assumption: `name` is mandatory
	 */
	public static class Builder	{
		private int id;
		private List<String> keywords;
		private String name;

		public Builder(String name) {
			this.name = name;
		}

		public Builder withId(int id)	{
			this.id = id;
			return this;
		}

		public Builder withKeywords(List<String> keywords)	{
			this.keywords = keywords;
			return this;
		}

		public Topic build()	{
			return new Topic(this);
		}
	}
}