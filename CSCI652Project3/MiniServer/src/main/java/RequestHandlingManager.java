import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

/**
 * A collection of all request handler supported by the MiniServer
 */
public class RequestHandlingManager {

    private static RequestHandlingManager requestHandlingManager;

    public static RequestHandlingManager getInstance()   {
        if (requestHandlingManager == null)    {
            requestHandlingManager = new RequestHandlingManager();
        }
        return requestHandlingManager;
    }

    public RequestHandler getMarkingNodeOnlineHandler() {
        return new MarkNodeOnlineHandler();
    }

    public RequestHandler getMarkingNodeOfflineHandler() {
        return new MarkNodeOfflineHandler();
    }

    public RequestHandler getOnlinePeerGuuidHandler()   {
        return new OnlinePeerGuuidHandler();
    }
}

/**
 * Mark a node online
 */
class MarkNodeOnlineHandler implements RequestHandler {

    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.MINISERVER_RPC_METHOD_NAME_MARK_ONLINE};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        int nodeGuuid = Integer.parseInt(request.getID().toString());
        String host = request.getNamedParams().getOrDefault(BaseConfig.RPC_PARAM_HOST, "").toString();
        PeerStatusManager.getInstance().markOnline(new Guuid(nodeGuuid, Guuid.getBinaryString(nodeGuuid), host));;
        return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
    }
}

/**
 * Mark a node offline
 */
class MarkNodeOfflineHandler implements RequestHandler {

    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.MINISERVER_RPC_METHOD_NAME_MARK_OFFLINE};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        Guuid toMarkOffline = Guuid.fromObject(request.getID().toString());
        ConsoleHelper.print("attempting to mark offline: " + toMarkOffline);
        PeerStatusManager.getInstance().markOffline(toMarkOffline);
        return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
    }
}

/**
 * Supply a list of Guuids for the peers which are marked as online
 */
class OnlinePeerGuuidHandler implements RequestHandler {
    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.MINISERVER_RPC_METHOD_NAME_GET_ONLINE_PEER_GUUID};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        ConsoleHelper.print("received request from id: ", request.getID().toString());
        // mark the incoming peer's guuid as online
        Guuid senderGuuid = Guuid.fromObject(request.getID().toString());
        ConsoleHelper.print("sender guuid: ", senderGuuid.toString());
        PeerStatusManager.getInstance().markOnline(senderGuuid);
        ConsoleHelper.print("marked online");
        // return topics from the topic manager
        return new JSONRPC2Response(PeerStatusManager.getInstance().getOnlinePeers(), request.getID());
    }
}