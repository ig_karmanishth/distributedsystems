import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Takes care of all Peer functions
 *
 * author: Ishan Guliani in.linkedin.com/in/ishanguliani, github.com/ishanguliani
 *
 * revision: 0
 *
 * version: 1.0
 */
public class KademliaPeerManager implements Peer {
    public static final String TAG = KademliaPeerManager.class.getSimpleName();
    private RoutingTable mRoutingTable;
    private RpcServerWorkerThreadManager rpcServerWorkerThreadManager;

    public static void main(String[] args) {
        new KademliaPeerManager().startWorking();
    }

    public void startWorking() {
        initializeRoutingTable();
    }

    public void startAllServices() {
        startReceivePendingKeysService();
        startListeningToFileStorageRequestsFromClients();
        startListeningToFileGetRequestsFromClients();
        startListeningToFileStoreHopRequest();
        startServiceToReceiveHelloPings();
        startBackupListeningService();
        startMenuDisplayService();
    }

    private void startBackupListeningService() {
        new Thread(() -> {
            try (ServerSocket listener = new ServerSocket(BaseConfig.KADEMLIA_PEER_BACKUP_KEY_PORT))	{
                ConsoleHelper.print(TAG, "service started successfully!", "BackupListeningService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    rpcServerWorkerThreadManager.getBackupRequestListeningWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void startServiceToReceiveHelloPings() {
        new Thread(() ->	{
            try (ServerSocket listener = new ServerSocket(BaseConfig.KADEMLIA_PEER_PING_HELLO_PORT))	{
                ConsoleHelper.print(TAG, "service started successfully!", "ReceiveHelloPings on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    rpcServerWorkerThreadManager.getHelloPingProcessingWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void startListeningToFileStorageRequestsFromClients() {
        new Thread(() ->	{
            try (ServerSocket listener = new ServerSocket(BaseConfig.KADEMLIA_PEER_CLIENT_FILE_STORE_PORT))	{
                ConsoleHelper.print(TAG, "service started successfully!", "ListeningToFileStorageRequestsFromClients on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    rpcServerWorkerThreadManager.getStoreFileWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void startListeningToFileGetRequestsFromClients() {
        new Thread(() ->	{
            try (ServerSocket listener = new ServerSocket(BaseConfig.KADEMLIA_PEER_CLIENT_FILE_GET_PORT))	{
                ConsoleHelper.print(TAG, "service started successfully!", "ListeningToFileGetRequestsFromClients on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    rpcServerWorkerThreadManager.getGetRequestFileWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void startListeningToFileStoreHopRequest() {
        new Thread(() ->	{
            try (ServerSocket listener = new ServerSocket(BaseConfig.KADEMLIA_PEER_FILE_STORE_REQUEST_HOP_PORT))	{
                ConsoleHelper.print(TAG, "service started successfully!", "ListeningToFileStoreHopRequest on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    rpcServerWorkerThreadManager.getFileStoreHopRequestWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void initializeRoutingTable() {
        rpcServerWorkerThreadManager = RpcServerWorkerThreadManager.getInstance();
        ConsoleHelper.print("Do you already have a unique Guuid for the peer ? If yes please enter it now else PRESS ENTER to auto generate a unique guuid for you");
        Scanner scanner = new Scanner(System.in);
        String response = scanner.nextLine();
        if (!response.isEmpty())    {
            try {
                int guuid = Integer.parseInt(response);
                Guuid currentPeer = new Guuid(guuid, Guuid.getBinaryString(guuid), InetAddress.getLocalHost().getHostAddress());
                mRoutingTable = RoutingTable.getInstance(currentPeer);
                startAllServices();
                mRoutingTable.refresh();
                mRoutingTable.sayHelloToPeers();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                ConsoleHelper.exit();
            }
        }
    }

    private void startReceivePendingKeysService() {
        new Thread(() ->	{
            try (ServerSocket listener = new ServerSocket(BaseConfig.KADEMLIA_PEER_TRANSFER_PENDING_KEYS_TO_OWNER_PORT))	{
                ConsoleHelper.print(TAG, "service started successfully!", "ReceivePendingKeysService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    rpcServerWorkerThreadManager.getReceivePendingKeysWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * A menu to ask the user what they want to do -
     * For instance -
     * 1. Lookup a node
     * 2. Store a file hash
     * 3. Get a file hash
     *
     * This service just runs on the main thread
     */
    private void startMenuDisplayService() {
        new Thread(() -> {
        String menu = "Press 0 at any time to safely exit the system.";
        int choice = ConsoleHelper.getUserChoiceFromCli(menu);
        switch (choice) {
            case 0:
                safelyExit();
                break;
            default:
                startMenuDisplayService();
                break;
        }}).start();
    }

    /**
     * Redistribute the contents of the current StorageManager to corresponding closest neighbour
     */
    private void safelyExit() {
        mRoutingTable.backupKeysBeforeDying();
        if (StorageManager.getInstance().isEmpty()) {
            ConsoleHelper.exit();
        } else  {
            ConsoleHelper.printError("it looks like there are still some keys left. Cannot exit!");
        }
    }

    /*    *//**
     * start a service to join the p2p network
     *//*
    private void startNetworkJoiningService() {
        new Thread(() -> {
            try (ServerSocket listener = new ServerSocket(BaseConfig.PORT_PEER_LOOKUP)) {
                ConsoleHelper.print(TAG, "service started!", "PeerLookupService on:", InetAddress.getLocalHost().getHostAddress(), ":", Integer.toString(listener.getLocalPort()));
                while (true) {
                    rpcServerWorkerThreadManager.getSaveAdvertisedTopicWorker(listener.accept()).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }*/

    @Override
    public String insert(File file) {
        return null;
    }

    @Override
    public File lookup(String hashCode) {
        return null;
    }
}
