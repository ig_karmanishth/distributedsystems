# Summary
Space-time independent asynchronous system with a central always up event manager. Event manager node caches pending notifications and delivers them when subscribers are back up online.

Tools/Knowledge Areas: docker, docker-compose, java8, socket programming, tcp/ip, udp, multi-threading, thread-pools, async programming


# System Design Specifications

## Project Structure
![](https://www.lucidchart.com/publicSegments/view/2fc67b03-1060-40fc-9de1-2361a43f2ac6/image.png)

## Architecture and Interactions
![](https://www.lucidchart.com/publicSegments/view/e73f09c7-cef2-4939-82a1-81c0dc3a0adb/image.png)

## Threads and port mappings
![](https://www.lucidchart.com/publicSegments/view/d4b50463-8f03-467c-9539-2221820cfb2b/image.png)

# About
 ```
author:         Ishan Guliani (ig_karmanishth)
Components:     1 network (i.e csci652network_custom_ig_karmanishth) and 3 services (i.e event_manager_service, publisher_service and subscriber_service)
Versions:       Docker Desktop 2.1.0.5(40693), Dockerfile v3.7, docker-compose 1.25.2
Docker daemon config(tested on): 4 CPUs, 6 GB Memory, 3 GB Swap
 ```

 # NOTES
   - `Publisher`, `Subscriber`, `EventManager` and  `BaseConfig` are completely independent maven modules that agree to certain protocol
   - `event_manager_service` must be up and completely running before triggering any publish/subscribe request
   - one should manually attach to appropriate containers (as mentioned in the steps below) to start various instances of Pubs and Subscribers

# Instructions to fire up containers with docker-compose
- `cd` into the current project directory *CSCI652Project2* which contains the maven modules _EventManager_, _Publisher_, _Subscriber_ and the _docker-compose_ config file
- be sure that the address space `172.20.240.0/24` is available and that the docker daemon has enough resources as [mentioned above](#about)<br>`$ docker-compose up --build -d --scale subscriber_service=2 --scale publisher_service=2`
- you should now have a total of 5 containers running (1 event manager, 2 publishers and 2 subscribers) which can be seen as follows<br>`$ docker container ls`<br>![](https://static.wixstatic.com/media/0a127f_a172fa64b49a42c284973835eca5c405~mv2.png)

### Step 1: attach to the event manager in one terminal session and make sure that the program is up and running
  - `$ docker attach event_manager_service`
  1. NOTE: Here is how a compiled and running EventManager looks like:<br>![](https://static.wixstatic.com/media/0a127f_aeaf83aca8154d2f9f0319b9db601613~mv2.png)
  2. The EventManager shows a running log of all activities happening in the PubSub system<br>NOTE: Steps [2,5] are space and time independent and may happen in any order or concurrently 

### Step 2: Attaching to new publisher: attach to a publisher service in a new terminal session
  - `$ docker attach csci652project2_publisher_service_1`
  1. Starting program: Once inside the container, start interacting with the publisher through the console input stream <br>`$ java -cp Publisher/target/Publisher-1.0-SNAPSHOT.jar PublisherManager`
  2. You should now be able to see an interactive menu that you can start interacting with.<br>![](https://static.wixstatic.com/media/0a127f_9d20ebd97d2f4d509cefb626371b8b23~mv2.png)

### Step 3: Attaching to new publisher: attach to a new publisher service in a new terminal session
  - `$ docker attach csci652project2_publisher_service_2`
  1. Starting program: Once inside the container, start interacting with the publisher through the console input stream<br>`$ java -cp Publisher/target/Publisher-1.0-SNAPSHOT.jar PublisherManager`
  2. You should now be able to see a similar interactive menu that you can start interacting with.<br>![](https://static.wixstatic.com/media/0a127f_9d20ebd97d2f4d509cefb626371b8b23~mv2.png)

### Step 4: Attaching to new subscriber: attach to a subscriber service in a terminal session
  - `$ docker attach csci652project2_subscriber_service_1`
  1. Starting program: Once inside the container, start interacting with the subscriber through the console input stream<br>`$ java -cp Subscriber/target/Subscriber-1.0-SNAPSHOT.jar SubscriberManager`
  2. Logging in: You will be _optionally_ prompted to enter a _subscriber id_ if you already know it, <br>or press RETURN to continue logging in with a globally unique ID for this subscriber<br>`$ java -cp Subscriber/target/Subscriber-1.0-SNAPSHOT.jar SubscriberManager`
  3. Console output:<br>![](https://static.wixstatic.com/media/0a127f_387cfe4722604d049a1311d2567dcd35~mv2.png)
  4. Start interacting with menu: You can now begin testing the program with the following menu options:<br>![](https://static.wixstatic.com/media/0a127f_43282bcbff84430cb6729f77849ffa9f~mv2.png)

### Step 5: Attaching to new subscriber: attach to a new subsriber service in a new terminal session
  - `$ docker attach csci652project2_subscriber_service_2`
  - NOTE: proceed with testing in a similar fashion

# Common errors
Error:      ```ERROR: Pool overlaps with other one on this address space``` <br>
Solution#1:   Restart the docker daemon <br>
Solution#2:   Run `$ docker network ls` and find the ID of the running instance of network beginning with _csci652network*_. Then remove it with `$ docker network rm <ID>` and try running `$ docker-compose up --build` again <br>
Solution#3:   Update the subnet to a more available subnet in _lines 21, 28, 37 and 52 of docker-compose.yml_
