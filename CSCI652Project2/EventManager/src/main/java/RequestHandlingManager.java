import com.fasterxml.jackson.databind.ObjectMapper;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

import java.io.IOException;
import java.util.List;

/**
 * A collection of all request handler supported by the Event Manager
 */
public class RequestHandlingManager {

    private static RequestHandlingManager requestHandlingManager;

    public static RequestHandlingManager getInstance()   {
        if (requestHandlingManager == null)    {
            requestHandlingManager = new RequestHandlingManager();
        }
        return requestHandlingManager;
    }

    public TopicAdvertisementHandler getTopicAdvertisementHandler() {
        return new TopicAdvertisementHandler();
    }

    public TopicSupplierHandler getTopicSupplierHandler() {
        return new TopicSupplierHandler();
    }

    public RequestHandler getLoginRequestHandler() {
        return new LoginRequestHandler();
    }

    public RequestHandler getSubscriptionRequestHandler() {
        return new SubscriptionRequestHandler();
    }

    public RequestHandler getUnsubscriptionRequestHandler() {
        return new UnsubscriptionRequestHandler();
    }

    public RequestHandler getEventRequestHandler() {
        return new EventRequestHandler();
    }

    public PendingNotificationsRequestHandler getPendingNotificationsRequestHandler() {
        return new PendingNotificationsRequestHandler();
    }
}

/**
 * Extract new topics from incoming requests and return them
 */
class TopicAdvertisementHandler implements RequestHandler {
    private static final String TAG = TopicAdvertisementHandler.class.getSimpleName();
    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.RPC_METHOD_NAME_ADVERTISE_TOPIC};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        Object topicObject = request.getNamedParams().get(BaseConfig.RPC_PARAM_TOPIC);
        if (topicObject == null) {
            ConsoleHelper.printError(TAG, "topicObject in params is null");
            return new JSONRPC2Response(JSONRPC2Error.INVALID_PARAMS, request.getID());
        }
        // run some logic to store this new topic in the hashmap
        ConsoleHelper.printError(TAG, "found topic:", topicObject.toString());
        return new JSONRPC2Response(topicObject, request.getID());
    }
}

/**
 * Supply a list of topics
 */
class TopicSupplierHandler implements RequestHandler {
    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.RPC_METHOD_NAME_GET_TOPICS};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        // return topics from the topic manager
        return new JSONRPC2Response(TopicManager.getInstance().getTopics(), request.getID());
    }
}

/**
 * Login a new subsriber
 */
class LoginRequestHandler implements RequestHandler {
    private static final String TAG = LoginRequestHandler.class.getSimpleName() + ":";
    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.RPC_METHOD_NAME_LOGIN};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        // add the incoming subscriber to the subscription manager
        int newSubscriberId = Integer.parseInt(request.getID().toString());
        Object newSubscriberHostIpObject = request.getNamedParams().getOrDefault(BaseConfig.RPC_PARAM_HOST_IP, null);
        SubscriberModel newSubscriber = new SubscriberModel(newSubscriberId, newSubscriberHostIpObject.toString(), true);
        SubscriptionManager.getInstance().addSubscriber(newSubscriber);
        ConsoleHelper.print(TAG, "New Subscriber login request:", newSubscriber.toString());
        return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
    }
}

/**
 * Adhere to incoming request from subcriber to subscribe to a topic
 */
class SubscriptionRequestHandler implements RequestHandler {
    private static final String TAG = SubscriptionRequestHandler.class.getSimpleName() + ":";

    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.RPC_METHOD_NAME_SUBSCRIBE_TOPIC};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        int subscriberId = Integer.parseInt(request.getID().toString());
        SubscriberModel requestingSubscriber = SubscriptionManager.getInstance().getSubscriberFromId(subscriberId);
        Object topicObject = request.getNamedParams().getOrDefault(BaseConfig.RPC_PARAM_TOPIC, null);
        if (requestingSubscriber == null || topicObject == null)    {
            ConsoleHelper.printError(TAG, "No valid topic or subscriber received in the request:", request.toString());
            return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE.getValue(), request.getID());
        }
        try {
            Topic requestedTopic = new ObjectMapper().readValue(topicObject.toString(), Topic.class);
            // register new topic subscription request
            SubscriptionManager.getInstance().addSubscriptionMapping(requestingSubscriber, requestedTopic);
            return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
        } catch (IOException e) {
            e.printStackTrace();
            return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE.getValue(), request.getID());
        }
    }
}

/**
 * Adhere to incoming request from subcriber to subscribe to a topic
 */
class UnsubscriptionRequestHandler implements RequestHandler {
    private static final String TAG = UnsubscriptionRequestHandler.class.getSimpleName() + ":";

    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.RPC_METHOD_NAME_UNSUBSCRIBE_TOPIC, BaseConfig.RPC_METHOD_NAME_UNSUBSCRIBE_ALL};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        int subscriberId = Integer.parseInt(request.getID().toString());
        SubscriberModel requestingSubscriber = SubscriptionManager.getInstance().getSubscriberFromId(subscriberId);
        if (requestingSubscriber == null) {
            ConsoleHelper.printError(TAG, "failure! Invalid subscriber received in the request:", request.toString());
            return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE.getValue(), request.getID());
        }
        if (request.getMethod().equals(BaseConfig.RPC_METHOD_NAME_UNSUBSCRIBE_TOPIC)) {
            Object topicObject = request.getNamedParams().getOrDefault(BaseConfig.RPC_PARAM_TOPIC, null);
            if (topicObject == null) {
                ConsoleHelper.printError(TAG, "No valid topic or subscriber received in the request:", request.toString());
                return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE.getValue(), request.getID());
            }
            try {
                Topic topicToUnsubscribeFrom = new ObjectMapper().readValue(topicObject.toString(), Topic.class);
                // remove given topic subscription for this subscriber
                SubscriptionManager.getInstance().removeSubscriptionMapping(requestingSubscriber, topicToUnsubscribeFrom);
            } catch (IOException e) {
                e.printStackTrace();
                return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE.getValue(), request.getID());
            }
        } else if (request.getMethod().equals(BaseConfig.RPC_METHOD_NAME_UNSUBSCRIBE_ALL)) {
            // remove all topic subscriptions for this subscriber
            SubscriptionManager.getInstance().removeSubscriptionMapping(requestingSubscriber);
        }
        return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
    }
}

/**
 * handle incoming event from publisher
 */
class EventRequestHandler implements RequestHandler {
    private static final String TAG = SubscriptionRequestHandler.class.getSimpleName() + ":";

    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.RPC_METHOD_NAME_PUBLISH_EVENT};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        Object eventObject = request.getNamedParams().getOrDefault(BaseConfig.RPC_PARAM_EVENT, null);
        if (eventObject == null)    {
            ConsoleHelper.printError(TAG, "No valid event received in the request:", request.toString());
            return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE.getValue(), request.getID());
        }
        try {
            Event incomingEvent = new ObjectMapper().readValue(eventObject.toString(), Event.class);
            // attempt to send the event notification to the registered subscribers
            SubscriptionManager.getInstance().notifySubscribers(incomingEvent);
            return new JSONRPC2Response(BaseConfig.ResponseEnum.SUCCESS.getValue(), request.getID());
        } catch (IOException e) {
            e.printStackTrace();
            return new JSONRPC2Response(BaseConfig.ResponseEnum.FAILURE.getValue(), request.getID());
        }
    }
}

/**
 * Supply list of pending notifications
 */
class PendingNotificationsRequestHandler implements RequestHandler {
    @Override
    public String[] handledRequests() {
        return new String[]{BaseConfig.RPC_METHOD_NAME_EVENT_NOTIFICATION};
    }

    @Override
    public JSONRPC2Response process(JSONRPC2Request request, MessageContext messageContext) {
        // return pending notifications
        int subscriberId = Integer.parseInt(request.getID().toString());
        List<Event> pendingEventsToSendBack = PendingEventsManager.getInstance().getPendingEventsForSubscriberById(subscriberId);
        ConsoleHelper.print("subscriberId:", Integer.toString(subscriberId), ", pendingEventsToSendBack:", pendingEventsToSendBack.toString());
        return new JSONRPC2Response(pendingEventsToSendBack, request.getID());
    }
}