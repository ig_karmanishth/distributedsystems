import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class PublisherManager {
    public static void main(String[] args) {
        new PublisherWorker().startWorking();
    }
}

class PublisherWorker implements BaseInterface, PublisherInterface  {

    private static final String MESSAGE_PRESS_RETURN_FOR_RANDOM_ID = "[press RETURN to use random unique id: ";
    private static final String MESSAGE_PRESS_RETURN_TO_SKIP = "[press RETURN to skip]: ";
    private static final String MESSAGE_OPTIONAL = "[OPTIONAL]";
    private static final String MESSAGE_REQUIRED = "[REQUIRED]";
    private static final Long DEFAULT_TTL = 60000L;

    @Override
    public void startWorking()  {
        int userInput = 0;
        do {
            String cliMessageForTheUser = getMenuMessage();
            userInput = ConsoleHelper.getUserChoiceFromCli(cliMessageForTheUser);
            processInput(userInput);
        } while (userInput != 0);
    }

    private void processInput(int userInput) {
        switch (userInput)  {
            case 1:
                Topic newTopic = getNewTopicFromUser();
                advertise(newTopic);
                break;
            case 2:
                Event newEvent = getNewEventFromUser();
                if (newEvent != null) {
                    publish(newEvent);
                }
                break;
            default:
                break;
        }
    }

    /**
     * Prompt the user for inputs and return a topic object with the following fields
     * - name: required
     * - list of keywords: optional
     * - uniqueID: optional, defaults to a unique hash value if not entered by the user
     * @return  a new Topic using builder pattern
     */
    private Topic getNewTopicFromUser() {
        Scanner scanner = new Scanner(System.in);
        ConsoleHelper.printLine();
        ConsoleHelper.print("New Topic Creation Menu:");
        String topicName;
        do {
            ConsoleHelper.print("Enter Topic Name" + MESSAGE_REQUIRED + ":");
            topicName = scanner.nextLine();
        } while (topicName.isEmpty());
        ConsoleHelper.print("Enter a comma separated list of keywords for the topic (example: crypto, bitcoin, etherium, blockchain)" + MESSAGE_OPTIONAL + MESSAGE_PRESS_RETURN_TO_SKIP);
        String keywordString = scanner.nextLine();
        List<String> keywordList = Collections.emptyList();
        if (!keywordString.isEmpty())   {
            // split keywords by comma and collect them in a list after clearing all extra white space
            keywordList = Arrays.stream(keywordString.split(","))
                    .peek(keyword -> keyword.replace("\\s+", ""))
                    .collect(Collectors.toList());
        }
        // assign a default unique ID
        int uniqueId = Math.abs(Objects.hash(topicName, keywordList)%10000);
        ConsoleHelper.print("Enter ID" + MESSAGE_OPTIONAL + MESSAGE_PRESS_RETURN_FOR_RANDOM_ID + uniqueId%1000000 + "]: ");
        String id = scanner.nextLine();
        if (!id.isEmpty()) {
            uniqueId = Integer.parseInt(id);
        }
        return new Topic.Builder(topicName)
                .withId(uniqueId)
                .withKeywords(keywordList)
                .build();
//        return new Topic(uniqueId, keywordList, topicName);
    }

    /**
     * Prompt the user and get a new Event object with the following fields-
     * - topic: required, user is shown a list of available topics and they select which one to use
     * -
     * @return
     */
    private Event getNewEventFromUser() {
        Scanner scanner = new Scanner(System.in);
        ConsoleHelper.printLine();
        ConsoleHelper.print("New Event Creation Menu:");
        Topic associatedTopic;
        RpcClient rpcClient = RpcClient.getNew(BaseConfig.IP_EVENT_MANAGER + ":" + BaseConfig.PORT_EVENT_MANAGER_GET_TOPICS);
        List<Topic> topicListFromEventManager = rpcClient.getTopics(1);
        if (topicListFromEventManager.isEmpty())    {
            ConsoleHelper.print("No topics available at this moment! Try again later.");
            return null;
        }
        // take topic name
        while (true)    {
            ConsoleHelper.print("Here are the available topics: ");
            HashMap<Integer, Topic> topicMenuMap = new HashMap<>();
            int sno = 1;
            for (Topic topic : topicListFromEventManager)  {
                topicMenuMap.put(sno, topic);
                sno += 1;
            }
            // show the menu to the user
            topicMenuMap.forEach((key, value) -> ConsoleHelper.print(key.toString(), ".", value.toString()));
            ConsoleHelper.print("Enter the sno to select a topic: ");
            try {
                int choice = Integer.parseInt(scanner.nextLine());
                associatedTopic = topicMenuMap.getOrDefault(choice, null);
                if (associatedTopic != null) {
                    break;
                }
            } catch (NumberFormatException ignored) {
            }
            ConsoleHelper.printError("Oops! That seems to be an invalid input. Try again.");
        }
        // generate a default unique id based on (event name, current time)
        int uniqueId = Math.abs(Objects.hash(associatedTopic, Instant.now())%10000);
        ConsoleHelper.print("Enter ID" + MESSAGE_OPTIONAL + MESSAGE_PRESS_RETURN_FOR_RANDOM_ID + uniqueId % 1000000 + "]: ");
        while (true) {
            try {
                String input = scanner.nextLine();
                if (!input.isEmpty())    {
                    uniqueId = Integer.parseInt(input);
                }
                break;
            } catch (NumberFormatException ignored) {
                ConsoleHelper.printError("Oops! An integer value is expected. Please try again!");
            }
        }
        ConsoleHelper.print("event id:", Integer.toString(uniqueId));

        // take event title
        String title = "";
        while (true)    {
            ConsoleHelper.print("Enter event title" + MESSAGE_REQUIRED + ":");
            title = scanner.nextLine();
            if (title.isEmpty()) {
                ConsoleHelper.printError("Oops! An event must have a title. Try again.");
            } else {
                break;
            }
        }
        // take event content
        ConsoleHelper.print("Enter event content" + MESSAGE_OPTIONAL + MESSAGE_PRESS_RETURN_TO_SKIP);
        String content = scanner.nextLine();
        // take event TTL (optional)
        long ttl = DEFAULT_TTL;
        ConsoleHelper.print("Enter event TTL (in milliseconds)" + MESSAGE_OPTIONAL + "[press RETURN to use default value of " + DEFAULT_TTL + " ]: ");
        String input = scanner.nextLine();
        if (!input.isEmpty())    {
            ttl = Long.parseLong(input);
        }
        return new Event.Builder(associatedTopic, title)
                .withId(uniqueId)
                .withContent(content)
                .withTtl(ttl)
                .build();
    }

    public void publish(Event newEvent) {
        BaseConfig.QosCounter qosCounter = BaseConfig.getInstance().getNewQosCounter();
        while (qosCounter.isRetryAttemptAvailable()) {
            // get RPC client instance
            RpcClient rpcClient = RpcClient.getNew(BaseConfig.IP_EVENT_MANAGER + ":" + BaseConfig.PORT_EVENT_MANAGER_EVENT_PUBLISH);
            // send new event to event manager
            JSONRPC2Response response = null;
            try {
                response = rpcClient.publishNewEvent(newEvent, 2);
                boolean responseSuccess = Boolean.parseBoolean(response.getResult().toString());
                if (responseSuccess) {
                    ConsoleHelper.print("success! event published successfully:", newEvent.toString());
                    qosCounter.markSuccessfulTransmission();
                } else {
                    ConsoleHelper.printError("failed! event");
                    ConsoleHelper.printError("response.getError(): ", response.getError().getMessage());
                }
            } catch (JSONRPC2SessionException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Advertise new topic to event manager over rpc
     * @param newTopic
     */
    public void advertise(Topic newTopic) {
        // get RPC client instance
        RpcClient rpcClient = RpcClient.getNew(BaseConfig.IP_EVENT_MANAGER + ":" +  BaseConfig.PORT_EVENT_MANAGER_TOPIC_ADVERTISEMENT);
        // send new topic to event manager
        JSONRPC2Response response = rpcClient.advertiseNewTopic(newTopic, 0);
        ConsoleHelper.print("response.getId(): ", response.getID().toString());
        ConsoleHelper.print("response.getError() == null", Boolean.toString(response.getError() == null));
        if (response.indicatesSuccess()) {
            ConsoleHelper.print("response.getResult(): ", response.getResult().toString());
            ConsoleHelper.print("success! topic advertised successfully: ", newTopic.toString());
        } else {
            ConsoleHelper.printError("failed! topic");
            ConsoleHelper.printError("response.getError(): ");
        }
    }

    @Override
    public String getMenuMessage() {
        return "What would you like to do?\n" +
                "1. advertise new topic\n" +
                "2. publish an event()\n" +
                "0. exit()\n";
    }
}
