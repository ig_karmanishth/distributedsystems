import java.io.IOException;

public interface EventManagerInterface {
    /*
     * Start the repo service
     */
    public void startAllServices() throws IOException;

    /*
     * notify all subscribers of new event
     */
    public void notifySubscribers(Event event);

    /*
     * add new topic when received advertisement of new topic
     */
    public void addTopic(Topic topic);

    /*
     * add subscriber to the internal list
     */
    public void addSubscriber();

    /*
     * remove subscriber from the list
     */
    public void removeSubscriber();

    /*
     * show the list of subscriber for a specified topic
     */
    public void showSubscribers(Topic topic);
}

