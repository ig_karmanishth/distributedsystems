import java.util.ArrayList;
import java.util.List;

public class TopicManager {
    private static String TAG = TopicManager.class.getSimpleName();
    private List<Topic> topicList = new ArrayList<>();

    private static TopicManager topicManager;

    public static TopicManager getInstance()    {
        if (topicManager == null)   {
            topicManager = new TopicManager();
        }
        return topicManager;
    }

    public synchronized void addTopic(Topic topic) {
        this.topicList.add(topic);
        ConsoleHelper.print(TAG, "success! topic added: ", topic.toString());
        printTopicList();
    }

    public List<Topic> getTopics()   {
        return this.topicList;
    }

    public void printTopicList() {
        ConsoleHelper.printLine();
        ConsoleHelper.print("UPDATED TOPIC LIST");
        ConsoleHelper.printLine();
        this.topicList.forEach(System.out::println);
        ConsoleHelper.printLine();
    }
}
