import java.util.Objects;

/**
 * A model class represent a single instance of Subscriber
 */
public class SubscriberModel {
    int id;
    String ip;
    boolean activeStatus;

    public SubscriberModel(int id, String ip, boolean activeStatus) {
        this.id = id;
        this.ip = ip;
        this.activeStatus = activeStatus;
    }

    public int getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }

    public boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(boolean status) {
        this.activeStatus = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriberModel that = (SubscriberModel) o;
        return id == that.id &&
                activeStatus == that.activeStatus &&
                ip.equals(that.ip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ip, activeStatus);
    }

    @Override
    public String toString() {
        return "Subscriber{" +
                "id=" + id +
                ", ip='" + ip + '\'' +
                '}';
    }
}
